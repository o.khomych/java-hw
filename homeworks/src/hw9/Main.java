package hw9;

import libs.Console;
import libs.DayOfWeek;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "do home work");
        schedule.put(DayOfWeek.TUESDAY, "go to courses; watch a film");
        schedule.put(DayOfWeek.WEDNESDAY, "make a presentation");
        schedule.put(DayOfWeek.THURSDAY, "go to the jym");
        schedule.put(DayOfWeek.FRIDAY, "write an assay");
        schedule.put(DayOfWeek.SATURDAY, "go to the doctor");
        schedule.put(DayOfWeek.SUNDAY, "read a book");

        Woman LilyEvans = new Woman("Lily", "Evans", "25/05/1994", 124, schedule);
        Man MetthewJames = new Man("Metthew", "James", "01/02/1980", 115, schedule);

        Console.println(LilyEvans.toString());
        Console.println(LilyEvans.describeAge());
        Console.println(MetthewJames.toString());
        Console.println(MetthewJames.describeAge());

        Man child = new Man("Kolin", "Mist", "22/05/2021", 80);
    }
}
