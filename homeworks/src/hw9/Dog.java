package hw9;

import hw7.Species;
import libs.Console;

import java.util.Set;

public class Dog  extends Pet implements Foul {
    public Dog(String nickname, Integer age, Integer trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.DOG;
    }

    public Dog(String nickname) {
        super(nickname);
        this.species = Species.DOG;
    }

    @Override
    public void respond() {
        Console.println("Woof, my name is " + getNickname());
    }

    @Override
    public void foul() {
        Foul.super.foul();
    }
}
