package hw9;

import libs.DayOfWeek;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Family implements HumanCreator {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pet;
    public Family(Human mother, Human father){
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<Human>();
        this.pet = new HashSet<Pet>();
        mother.setFamily(this);
        father.setFamily(this);
    }
    public Human getMother(){return mother;}
    public Human getFather(){return father;}
    public List<Human> getChildren(){return children;}
    public Set<Pet> getPet(){return pet;}

    public void addPet(Pet newPet){
        pet.add(newPet);
    }
    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother.toString() +
                ", father=" + father.toString() +
                ", children=" + children +
                ", pet=" + pet +
                '}';
    }

    public void addChild(Human child){
        children.add(child);
        child.setFamily(this);
    }

    public boolean deleteChildIndex(int index){
        if (index > getChildren().size()-1) return false;
        getChildren().get(index).setFamily(null);
        getChildren().remove(index);
        return true;
    }

    public boolean deleteChildObj(Human child){
        int index = getChildren().indexOf(child);
        if(index == -1) return false;
        getChildren().get(index).setFamily(null);
        getChildren().remove(index);
        return true;
    }

    public int countFamily(){
        return children.size() + 2;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null) return false;
        if (!(that instanceof Family)) return false;
        Family family = (Family) that;
        if (!this.mother.equals(family.getMother())) return false;
        if (!this.father.equals(family.getFather())) return false;
        return true;
    }
    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    @Override
    public Human bornChild() {
        boolean isMale = Math.random() < 0.5;
        String name = isMale ?availableManNames[(int) (Math.random() * availableManNames.length)]: availableWomanNames[(int) (Math.random() * availableWomanNames.length)];
        Integer iq = ((this.getMother().getIq() == null) || (this.getFather().getIq() == null)) ? null : (this.getMother().getIq() + this.getFather().getIq())/2;
        Map<DayOfWeek, String>   schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "diaper change, nurse, play, nap");
        schedule.put(DayOfWeek.TUESDAY, "diaper change, nurse, play, nap");
        schedule.put(DayOfWeek.WEDNESDAY, "diaper change, nurse, play, nap");
        schedule.put(DayOfWeek.THURSDAY, "diaper change, nurse, play, nap");
        schedule.put(DayOfWeek.FRIDAY, "diaper change, nurse, play, nap");
        schedule.put(DayOfWeek.SATURDAY, "diaper change, nurse, play, nap");
        schedule.put(DayOfWeek.SUNDAY, "diaper change, nurse, play, nap");
        LocalDate birthday = LocalDate.now();
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String birthDay = birthday.format(formatters);
        Human child = isMale? new Man(name, this.getFather().getSurname(), birthDay, iq, schedule) :
                new Woman(name, this.getFather().getSurname(), birthDay, iq, schedule);
        addChild(child);
        return child;
    }


//    static {
//        Console.println("new Family class loaded");
//    }
//    {
//        Console.println("creating new object type Family");
//    }
protected void finalize() throws Throwable {
    System.out.println("Object: " +this+" is deleted");
}

}
