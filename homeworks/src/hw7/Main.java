package hw7;

import libs.Console;
import libs.DayOfWeek;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<String> habits = new HashSet<String>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");
        Pet catMarci = new DomesticCat("Marci",2,80, habits);
        System.out.println(catMarci.toString());
        Set<String> fishHabits = new HashSet<String>();
        fishHabits.add("eat");
        fishHabits.add("sleep");
        fishHabits.add("swim");
        Pet fishDory = new Fish("Dory",1,10, fishHabits);
        System.out.println(fishDory.toString());


        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "do home work");
        schedule.put(DayOfWeek.TUESDAY, "go to courses; watch a film");
        schedule.put(DayOfWeek.WEDNESDAY, "make a presentation");
        schedule.put(DayOfWeek.THURSDAY, "go to the jym");
        schedule.put(DayOfWeek.FRIDAY, "write an assay");
        schedule.put(DayOfWeek.SATURDAY, "go to the doctor");
        schedule.put(DayOfWeek.SUNDAY, "read a book");

        Woman LilyEvans = new Woman("Lily", "Evans", 1994, 124, schedule);
        Man MetthewJames = new Man("Metthew", "James", 190, 115, schedule);

        Family JamesFamily = new Family(LilyEvans, MetthewJames);
        JamesFamily.addPet(fishDory);
        JamesFamily.addPet(catMarci);
        JamesFamily.addPet(fishDory);
        Human child = JamesFamily.bornChild();
        Console.println(JamesFamily.toString());

        child.greetPet();
        child.feedPet(false, fishDory);
        child.feedPet(true, catMarci);
        child.describePet();
    }
}
