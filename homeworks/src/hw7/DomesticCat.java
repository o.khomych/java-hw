package hw7;

import libs.Console;

import java.util.Set;

public class DomesticCat extends Pet implements Foul {

    public DomesticCat(String nickname, Integer age, Integer trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.DomesticCat;
    }

    public DomesticCat(String nickname) {
        super(nickname);
        this.species = Species.DomesticCat;
    }

    @Override
    public void foul() {
        Foul.super.foul();
    }

    @Override
    public void respond() {
        Console.println(String.format("Привет, хозяин. Я - %s. Я соскучился!", getNickname()));
    }
}
