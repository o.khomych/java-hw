package hw7;

import libs.Console;
import libs.DayOfWeek;

import java.util.Map;

public final class Man extends Human {

    public Man(String name, String surname, Integer year, Integer iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, Integer year) {
        super(name, surname, year);
    }

    @Override
    public void greetPet() {
    for (Pet p: getFamily().getPet()) {
        Console.println(String.format("Привет, %s. Я купил тебе корм.", p.getNickname()));
    }
}
    public void repairCar(){
        Console.println("Отвезу машину в ремонт!");
    }
    @Override
    public String toString() {
        return "Man{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null) return false;
        if (!(that instanceof Man)) return false;
        Man man = (Man) that;
        if (!this.name.equals(man.name)) return false;
        if (!this.surname.equals(man.surname)) return false;
        if (!this.year.equals(man.year)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode()+surname.hashCode()+year.hashCode() +getClass().hashCode();
    }
}
