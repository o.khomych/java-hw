package hw7;

import libs.Console;
import libs.DayOfWeek;

import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class Human {
    protected String name;
    protected String surname;
    protected Integer year;
    protected Integer iq;
    protected Map<DayOfWeek, String> schedule;
    protected Family family;
    private Human(){

    }
    public Human(String name, String surname, Integer year, Integer iq, Map<DayOfWeek, String> schedule){
      this.name=name;
      this.surname=surname;
      this.year = year;
      this.iq = iq;
      this.schedule = schedule;
    }
    public Human(String name, String surname, Integer year){
        this(name, surname, year, null, null);
    }
    public String getName(){return name;}
    public void setName(String str){name = str;}
    public String getSurname(){return surname;}
    public void setSurname(String str){surname = str;}
    public Integer getYear(){return year;}
    public void setYear(Integer ints){year = ints;}
    public Integer getIq(){return iq;}
    public void setIq(Integer ints){iq = ints;}
    public Family getFamily(){return family;}
    public void setFamily(Family addFamily){family = addFamily;}
    public Map<DayOfWeek, String> getSchedule(){return schedule;}
    public void setSchedule(Map<DayOfWeek, String> strings){schedule=strings;}


    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null) return false;
        if (!(that instanceof Human)) return false;
        Human human = (Human) that;
        if (!this.name.equals(human.name)) return false;
        if (!this.surname.equals(human.surname)) return false;
        if (!this.year.equals(human.year)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year);
    }

//    static {
//        Console.println("new Human class loaded");
//    }
//    {
//        Console.println("creating new object type Human");
//    }

    public void greetPet(){
        for (Pet p: family.getPet()) {
            Console.println(String.format("Привет, %s.", p.getNickname()));
        }
    }
    public void describePet(){
        for (Pet p: family.getPet()) {
        String trickLevel = p.getTrickLevel()>=50 ? "очень хитрый" : "почти не хитрый";
        Console.println(String.format("У меня есть "+ p.getSpecies()+ ", " +
                "ему "+p.getAge()+" лет, " +
                "он "+ trickLevel + ". "));
        }
    }

    public boolean feedPet(boolean timeToFeed, Pet pet){
        if(!family.getPet().contains(pet)){
            Console.println("Это не ваш питомец");
            return false;
        };
        Random random = new Random();
        if (timeToFeed) {
            Console.println("Хм... покормлю ка я " + pet.getNickname());
            return true;
        } else{
            int randomNumber = random.nextInt(101);
            if (pet.getTrickLevel() > randomNumber) {
                Console.println("Хм... покормлю ка я " + pet.getNickname());
                return true;
            }
            Console.println("Думаю " + pet.getNickname() + " не голоден.");
            return false;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Object: " +this+" is deleted");
    }

}
