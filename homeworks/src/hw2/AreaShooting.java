package hw2;

import libs.Console;

import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;

public class AreaShooting {
    public static void main(String[] args) {
        Random random = new Random();
        IntStream intS = random.ints(2, 1, 6);
        int [] ints = intS.toArray();
        int column = ints[0];
        int row = ints[1];

        Console.println("Let the game begin!");
        boolean isWon = false;

        int[][] area = new int[5][5];

        while(!isWon){
            printArray(area);

            Console.print("Enter number of row: ");
            int y = getNumber();

            Console.print("Enter number of column: ");
            int x = getNumber();

            Console.println("");

            if (x == column && y == row) {
                area[y-1][x-1] = 2;
                printArray(area);
                isWon = true;
            } else{
                area[y-1][x-1] = 1;
                Console.println("Shoot!!! Sorry, wrong( Try again!");
                Console.println("");
            }
        }
        System.out.println("You have won!");
    }

    public static void printArray(int[][] arr){
        for (int i=0; i<arr.length+1; i++) {
            System.out.printf("%d | ", i);
        }
        System.out.println();
        for (int i=0; i<arr.length; i++) {
            System.out.printf("%d | ", i+1);
            for (int j=0; j<arr.length; j++) {
                System.out.printf("%s | ", fillCell(arr[i][j]));
            }
            System.out.println();
        }
    }
    public static String fillCell(int value) {
        if (value == 1) return "*";
        if (value == 2) return "x";
        return "-";
    }
    public static int getNumber(){
        Scanner sc = new Scanner(System.in);

         while (!sc.hasNextInt()) {
             Console.print("This is not a number. Please enter valid number: ");
             sc.next();
         }
         int a = sc.nextInt();

         while (a < 1 || a> 5) {
            Console.print("Please enter valid number: ");
            a = sc.nextInt();
         }
         return a;
    }
}

