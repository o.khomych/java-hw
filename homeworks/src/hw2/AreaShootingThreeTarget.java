package hw2;

import libs.Console;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;


public class AreaShootingThreeTarget {
    public static void main(String[] args) {

        int [][] target = getTarget();

        Console.println("Let the game begin!");
        boolean isWon = false;
        int[][] area = new int[5][5];

        boolean[] shootResult = new boolean[3];
        while(!isWon){
            printArray(area);

            Console.print("Enter number of row: ");
            int y = getNumber();

            Console.print("Enter number of column: ");
            int x = getNumber();

            Console.println("");

            int [] targetShoot = new int [] {y, x};
            boolean[] result = new boolean[3];

            for (int i = 0; i < target.length; i++) {
                result[i] = Arrays.equals(target[i], targetShoot);
                if (result[i]) shootResult[i] = true;
            }
            if (result[0] || result[1] || result[2]) {
                area[y-1][x-1] = 2;
                printArray(area);
                if (Arrays.equals(shootResult, new boolean[]{true, true, true})) {
                    isWon = true;
                } else {
                    Console.println("You hit the target. Try more!");
                    Console.println("");
                }
            } else{
                area[y-1][x-1] = 1;
                Console.println("Sorry, you are wrong( Try again!");
                Console.println("");
            }
        }
        System.out.println("You have won!");
    }
    public static void printArray(int[][] arr){
        for (int i=0; i<arr.length+1; i++) {
            System.out.printf("%d | ", i);
        }
        System.out.println();
        for (int i=0; i<arr.length; i++) {
            System.out.printf("%d | ", i+1);
            for (int j=0; j<arr.length; j++) {
                System.out.printf("%s | ", fillCell(arr[i][j]));
            }
            System.out.println();
        }
    }
    public static String fillCell(int value) {
        if (value == 1) return "*";
        if (value == 2) return "x";
        return "-";
    }
    public static int getNumber(){
        Scanner sc = new Scanner(System.in);

        while (!sc.hasNextInt()) {
            Console.print("This is not a number. Please enter valid number: ");
            sc.next();
        }
        int a = sc.nextInt();

        while (a < 1 || a> 5) {
            Console.print("Please enter valid number: ");
            a = sc.nextInt();
        }
        return a;
    }
    public static int[][] getTarget(){
        Random random = new Random();
        IntStream intS = random.ints(2, 1, 6);
        int [] ints = intS.toArray();
        boolean isRow = random.nextBoolean();

        int row = ints[0];
        int column = ints[1];

        int [][] target = new int[3][1];
        target[0] = new int[]{row, column};
        if (isRow){
            if (column<4) {
                target[1] = new int[]{row, column + 1};
                target[2] = new int[]{row, column + 2};
            } else{
                target[1] = new int[]{row, column - 1};
                target[2] = new int[]{row, column - 2};
            }
        } else{
            if (row<4) {
                target[1] = new int[]{row+1, column};
                target[2] = new int[]{row+2, column};
            } else{
                target[1] = new int[]{row-1, column};
                target[2] = new int[]{row-2, column};
            }
        }
//        System.out.println(Arrays.deepToString(target));
        return target;
    }
}
