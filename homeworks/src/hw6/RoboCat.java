package hw6;

import libs.Console;

public class RoboCat extends Pet{

    public RoboCat(String nickname, Integer age, Integer trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.RoboCat;
    }

    public RoboCat(String nickname) {
        super(nickname);
        this.species = Species.RoboCat;
    }

    @Override
    public void respond() {
        Console.println("I am robot cat, my name is " + getNickname());
    }
}
