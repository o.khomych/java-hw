package hw6;

import libs.Console;

public class Dog  extends Pet implements Foul{
    public Dog(String nickname, Integer age, Integer trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.DOG;
    }

    public Dog(String nickname) {
        super(nickname);
        this.species = Species.DOG;
    }

    @Override
    public void respond() {
        Console.println("Woof, my name is " + getNickname());
    }

    @Override
    public void foul() {
        Foul.super.foul();
    }
}
