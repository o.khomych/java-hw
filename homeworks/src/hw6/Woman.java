package hw6;

import libs.Console;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;

public final class Woman extends Human{
    public Woman(String name, String surname, Integer year, Integer iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman(String name, String surname, Integer year) {
        super(name, surname, year);
    }

    @Override
    public void greetPet() {
        Console.println(String.format("Hi, %s. ", getFamily().getPet().getNickname()));
    }

    public void makeup(){
        Console.println("Схожу в салон красоты");
    }
    @Override
    public String toString() {
        return "Woman{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }
}


