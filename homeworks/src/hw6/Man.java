package hw6;

import libs.Console;

import java.util.Arrays;

public final class Man extends Human{

    public Man(String name, String surname, Integer year, Integer iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, Integer year) {
        super(name, surname, year);
    }

    @Override
    public void greetPet() {
        Console.println(String.format("Привет, %s. Я купил тебе корм.", getFamily().getPet().getNickname()));
    }
    public void repairCar(){
        Console.println("Отвезу машину в ремонт!");
    }
    @Override
    public String toString() {
        return "Man{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }
}
