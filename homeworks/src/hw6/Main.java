package hw6;

import libs.Console;

public class Main {
    public static void main(String[] args) {
        Pet catMarci = new DomesticCat("Marci",2,80, new String[]{"eat", "drink", "sleep"});
        System.out.println(catMarci.toString());

        Pet fishDory = new Fish("Dory",1,10, new String[]{"eat"});
        System.out.println(fishDory.toString());


        Woman LilyEvans = new Woman("Lily", "Evans", 1994, 124, new String[][]{{"Sunday", "plan1"},{"Monday", "plan2"}});
        Man MetthewJames = new Man("Metthew", "James", 190, 115, new String[][]{{"Monday", "plan12"},{"Tuesday", "plan22"}});

        Family JamesFamily = new Family(LilyEvans, MetthewJames);
        JamesFamily.setPet(fishDory);
        Human child = JamesFamily.bornChild();
        Console.println(JamesFamily.toString());

        child.greetPet();
    }
}
