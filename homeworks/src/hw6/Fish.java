package hw6;

import libs.Console;

public class Fish extends Pet{

    public Fish(String nickname, Integer age, Integer trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.FISH;
    }
    public Fish(String nickname) {
        super(nickname);
        this.species = Species.FISH;
    }
    @Override
    public void respond() {
        Console.println("...");
    }

}
