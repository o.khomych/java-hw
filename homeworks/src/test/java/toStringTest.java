import hw5.*;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotEquals;

public class toStringTest {
    public Family newFamily(){
        Human woman = new Human("Hanna", "Smith", 1999);
        Human man = new Human("Tom", "Smith", 1998);
        Family family = new Family(woman, man);
        return family;
    }
    @Test
    public void testPetToString() {
        Pet pet = new Pet(Species.FISH, "Dory");;
        String expected = "FISH{nickname='Dory', age=null, trickLevel=null, habits=[], animal cannot fly, has 0 legs and hasn't fur}";
        assertEquals(expected, pet.toString());
    }
    @Test
    public void testHumanToString() {
        Human woman = new Human("Hanna", "Smith", 1999, 98, new String[][]{{"day1", "plan1"},{"day2", "plan2"}});
        String expected = "Human{name='Hanna', surname='Smith', year=1999, iq=98, schedule=[[day1, plan1], [day2, plan2]]}";
        assertEquals(expected, woman.toString());
    }
    @Test
    public void testFamilyToString() {
        Human woman = new Human("Hanna", "Smith", 1999, 98, new String[][]{{"day1", "plan1"},{"day2", "plan2"}});
        Human man = new Human("Tom", "Smith", 1998, 91, new String[][]{{"day1", "plan1"},{"day2", "plan2"}});
        Family family = new Family(woman, man);
        String expected = "Family{mother=Human{name='Hanna', surname='Smith', year=1999, iq=98, schedule=[[day1, plan1], [day2, plan2]]}, father=Human{name='Tom', surname='Smith', year=1998, iq=91, schedule=[[day1, plan1], [day2, plan2]]}, children=[], pet=null}";
        assertEquals(expected, family.toString());
    }

    @Test
    public void testCountFamily(){
        Family family = newFamily();
        Human child1 = new Human("Olive", "Smith", 2021);

        family.addChild(child1);
        assertEquals(3, family.countFamily());
    }

    @Test
    public void test1DeleteChildWithIndex(){
        Family family = newFamily();
        Human child1 = new Human("Olive", "Smith", 2021);
        Human child2 = new Human("Timothy", "Smith", 2021);

        family.addChild(child1);
        family.addChild(child2);
        Human[] childTest = new Human[1];
        childTest[0]=child2;
        assertTrue(family.deleteChildIndex(0));
        assertEquals(childTest, family.getChildren());
    }
    @Test
    public void test2DeleteChildWithIndex(){
        Family family = newFamily();
        Human child1 = new Human("Olive", "Smith", 2021);

        family.addChild(child1);
        Human[] childTest = family.getChildren();

        assertFalse(family.deleteChildIndex(1));
        assertEquals(family.getChildren(), childTest);
    }
    @Test
    public void test1DeleteChildWithObj(){
        Family family = newFamily();
        Human child1 = new Human("Olive", "Smith", 2021);
        Human child2 = new Human("Timothy", "Smith", 2021);

        family.addChild(child1);
        family.addChild(child2);
        Human[] childTest = new Human[1];
        childTest[0]=child2;
        assertTrue(family.deleteChildObj(child1));
        assertEquals(childTest, family.getChildren());
    }
    @Test
    public void test2DeleteChildWithObj(){
        Family family = newFamily();
        Human child1 = new Human("Olive", "Smith", 2021);
        Human child2 = new Human("Timothy", "Smith", 2021);

        family.addChild(child1);
        Human[] childTest = family.getChildren();

        assertFalse(family.deleteChildObj(child2));
        assertEquals(family.getChildren(), childTest);
    }
    @Test
    public void testAddChild(){
        Family family = newFamily();

        Human child1 = new Human("Olive", "Smith", 2021);
        Human child2 = new Human("Timothy", "Smith", 2021);

        assertEquals(0, family.getChildren().length);

        family.addChild(child1);
        assertEquals(1, family.getChildren().length);
        assertEquals(family, child1.getFamily());

        family.addChild(child2);
        assertEquals(2, family.getChildren().length);
        assertEquals(family, child2.getFamily());
    }
    @Test
    public void testEqualsPet(){
        Pet cat1 = new Pet(Species.CAT, "Marci",2,80, new String[]{"eat", "drink", "sleep"});
        Pet cat2 = new Pet(Species.FISH, "Marci",2,80, new String[]{"eat", "drink", "sleep"});
        Pet cat3 = new Pet(Species.CAT, "Marcy",2,80, new String[]{"eat", "drink", "sleep"});
        Pet cat4 = new Pet(Species.CAT, "Marci",2,80, new String[]{"eat", "drink", "sleep", "play"});
        Pet cat5 = new Pet(Species.CAT, "Marci",2,84, new String[]{"eat", "drink", "sleep"});
        Pet cat6 = new Pet(Species.CAT, "Marci",6,80, new String[]{"eat", "drink", "sleep"});
        Human cat7 = new Human("Marci", "None", 1977);
        Pet cat8 = new Pet(Species.CAT, "Marci",2,80, new String[]{"eat", "drink", "sleep"});
        assertFalse(cat1.equals(cat2));
        assertFalse(cat1.equals(cat3));
        assertTrue(cat1.equals(cat4));
        assertTrue(cat1.equals(cat5));
        assertFalse(cat1.equals(cat6));
        assertFalse(cat1.equals(cat7));
        assertTrue(cat1.equals(cat8));

        assertNotEquals(cat1.hashCode(), cat2.hashCode());
        assertNotEquals(cat1.hashCode(), cat3.hashCode());
        assertEquals(cat1.hashCode(), cat4.hashCode());
        assertEquals(cat1.hashCode(), cat5.hashCode());
        assertNotEquals(cat1.hashCode(), cat6.hashCode());
        assertNotEquals(cat1.hashCode(), cat7.hashCode());
        assertEquals(cat1.hashCode(), cat8.hashCode());
    }
    @Test
    public void testEqualsHuman(){
        Human man1 = new Human("Tom", "Swan", 1969, 98, new String[][]{ {DayOfWeek.MONDAY.name(),"do home work"}, {DayOfWeek.TUESDAY.name(),"go to courses; watch a film"}});
        Human man2 = new Human("Tom", "Swan", 1969, 98, new String[][]{ {DayOfWeek.MONDAY.name(),"do home work"}, {DayOfWeek.TUESDAY.name(),"go to courses; watch a film"}});
        Human man3 = new Human("Tomi", "Swan", 1969, 98, new String[][]{ {DayOfWeek.MONDAY.name(),"do home work"}, {DayOfWeek.TUESDAY.name(),"go to courses; watch a film"}});
        Human man4 = new Human("Tom", "Swap", 1969, 98, new String[][]{ {DayOfWeek.MONDAY.name(),"do home work"}, {DayOfWeek.TUESDAY.name(),"go to courses; watch a film"}});
        Human man5 = new Human("Tom", "Swan", 1968, 98, new String[][]{ {DayOfWeek.MONDAY.name(),"do home work"}, {DayOfWeek.TUESDAY.name(),"go to courses; watch a film"}});
        Human man6 = new Human("Tom", "Swan", 1969, 98, new String[][]{ {DayOfWeek.TUESDAY.name(),"go to courses; watch a film"}});
        Human man7 = new Human("Tom", "Swan", 1969, 110, new String[][]{ {DayOfWeek.MONDAY.name(),"do home work"}, {DayOfWeek.TUESDAY.name(),"go to courses; watch a film"}});
        Pet man8 = new Pet(Species.CAT, "Tom",2,80, new String[]{"eat", "drink", "sleep"});
        assertTrue(man1.equals(man2));
        assertFalse(man1.equals(man3));
        assertFalse(man1.equals(man4));
        assertFalse(man1.equals(man5));
        assertTrue(man1.equals(man6));
        assertTrue(man1.equals(man7));
        assertFalse(man1.equals(man8));

        assertEquals(man1.hashCode(), man2.hashCode());
        assertNotEquals(man1.hashCode(), man3.hashCode());
        assertNotEquals(man1.hashCode(), man4.hashCode());
        assertNotEquals(man1.hashCode(), man5.hashCode());
        assertEquals(man1.hashCode(), man6.hashCode());
        assertEquals(man1.hashCode(), man7.hashCode());
        assertNotEquals(man1.hashCode(), man8.hashCode());
    }
    @Test
    public void testEqualFamily(){
        Human woman = new Human("Hanna", "Smith", 1999);
        Human man = new Human("Tom", "Smith", 1998);
        Human woman1 = new Human("Hanna", "Smith", 2002);
        Human man1 = new Human("Tom", "Smith", 2000);

        Family testFamily = newFamily();
        Family family = new Family(woman1, man1);
        Family family1 = new Family(woman, man);
        family1.addChild(new Human("Olive", "Smith", 2021));

        Family family2 = new Family(woman, man);
        family2.setPet(new Pet(Species.CAT, "Marci",2,80, new String[]{"eat", "drink", "sleep"}));

        assertTrue(testFamily.equals(new Family(woman, man)));
        assertFalse(testFamily.equals(family));
        assertFalse(testFamily.equals(new Family(woman1, man)));
        assertFalse(newFamily().equals(new Family(woman, man1)));
        assertTrue(testFamily.equals(family1));
        assertTrue(testFamily.equals(family2));

        assertEquals(testFamily.hashCode(), (new Family(woman, man)).hashCode());
        assertNotEquals(testFamily.hashCode(), family.hashCode());
        assertNotEquals(testFamily.hashCode(), (new Family(woman1, man)).hashCode());
        assertNotEquals(testFamily.hashCode(), (new Family(woman, man1)).hashCode());
        assertEquals(testFamily.hashCode(), family1.hashCode());
        assertEquals(testFamily.hashCode(), family2.hashCode());
    }

}
