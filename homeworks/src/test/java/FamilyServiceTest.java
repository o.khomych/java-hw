import hw8.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FamilyServiceTest {
    public Family newFamily(){
        Human woman = new Human("Hanna", "Smith", 1999);
        Human man = new Human("Tom", "Smith", 1998);
        return new Family(woman, man);
    }
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }
    @Test
    public void testGetAllFamilies(){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);

        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));
        List<Family> allFamilies = fs.getAllFamilies();

        assertEquals(1, allFamilies.size());
        assertEquals(newFamily(), allFamilies.get(0));
    }

    @Test
    public void testDisplayAllFamilies(){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);

        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));

        String expected = "0: Family{mother=Woman{name='Hanna', surname='Smith', year=1999, iq=null, schedule=null}, father=Man{name='Tom', surname='Smith', year=1998, iq=null, schedule=null}, children=[], pet=[]}";

        fs.displayAllFamilies();
        String actual = outContent.toString().trim();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetFamiliesBiggerThan(){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);
        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));
        fs.createNewFamily(new Woman("Karol", "Tong", 1999), new Man("Tom", "Tong", 1998));
        fs.createNewFamily(new Woman("Nina", "Storm", 1999), new Man("Dan", "Storm", 1998));
        List<Family> allFamilies = fs.getAllFamilies();
        fs.bornChild(allFamilies.get(0), "Adam", "Eva");
        fs.bornChild(allFamilies.get(0), "Alex", "Cara");

        List<Family> expectedFamilies = new ArrayList<Family>();
        expectedFamilies.add(allFamilies.get(0));
        List<Family> familiesBiggerThan = fs.getFamiliesBiggerThan(3);

        assertEquals(expectedFamilies, familiesBiggerThan);

        String printMesssage = expectedFamilies.toString();
        assertEquals(printMesssage, outContent.toString().trim());
    }
    @Test
    public void testGetFamiliesLessThan (){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);
        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));
        fs.createNewFamily(new Woman("Karol", "Tong", 1999), new Man("Tom", "Tong", 1998));
        fs.createNewFamily(new Woman("Nina", "Storm", 1999), new Man("Dan", "Storm", 1998));
        List<Family> allFamilies = fs.getAllFamilies();
        fs.bornChild(allFamilies.get(0), "Adam", "Eva");
        fs.bornChild(allFamilies.get(0), "Alex", "Cara");

        List<Family> expectedFamilies = new ArrayList<Family>();
        expectedFamilies.add(allFamilies.get(1));
        expectedFamilies.add(allFamilies.get(2));
        List<Family> familiesLessThan = fs.getFamiliesLessThan(3);

        assertEquals(expectedFamilies, familiesLessThan);

        String printMesssage = expectedFamilies.toString();
        assertEquals(printMesssage, outContent.toString().trim());
    }
    @Test
    public void testCountFamiliesWithMemberNumber (){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);
        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));
        List<Family> allFamilies = fs.getAllFamilies();
        fs.bornChild(allFamilies.get(0), "Adam", "Eva");

        assertEquals(1, fs.countFamiliesWithMemberNumber(3));
        assertEquals(0, fs.countFamiliesWithMemberNumber(2));
    }
    @Test
    public void testCreateNewFamily(){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);

        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));
        List<Family> allFamilies = fs.getAllFamilies();
        assertEquals(newFamily(), allFamilies.get(0));
        assertEquals(1, allFamilies.size());
    }
    @Test
    public void testDeleteFamilyByIndex (){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);

        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));
        List<Family> allFamilies = fs.getAllFamilies();
        assertEquals(1, allFamilies.size());
        boolean b = fs.deleteFamilyByIndex(0);
        assertEquals(0, allFamilies.size());
        assertTrue(b);
    }
    @Test
    public void testBornChild(){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);
        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));
        List<Family> allFamilies = fs.getAllFamilies();

        Family familySmith = fs.bornChild(allFamilies.get(0), "Adam", "Eva");
        assertEquals(3, familySmith.countFamily());
        Family family1 = newFamily();
        family1.addChild(new Man("Adam", "Smith", 2023, null, null));
        Family family2 = newFamily();
        family2.addChild(new Woman("Eva", "Smith", 2023, null, null));

        if(familySmith.getChildren().get(0) instanceof Man){
            assertEquals(family1.getChildren().get(0), familySmith.getChildren().get(0));
        } else{
            assertEquals(family2.getChildren().get(0), familySmith.getChildren().get(0));
        }

    }
    @Test
    public void testAdoptChild (){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);
        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));
        List<Family> allFamilies = fs.getAllFamilies();
        Family familySmith = allFamilies.get(0);
        Woman adoptedChild = new Woman("Gabriel", "Dorn", 2018);

        familySmith = fs.adoptChild(familySmith, adoptedChild);

        Family family = newFamily();
        family.addChild(new Woman("Gabriel", "Dorn", 2018));

        assertEquals(family, familySmith);

    }
    @Test
    public void testDeleteAllChildrenOlderThen (){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);
        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));
        List<Family> allFamilies = fs.getAllFamilies();
        Family familySmith = allFamilies.get(0);

        fs.adoptChild(familySmith, new Woman("Gabriel", "Dorn", 2018));
        fs.bornChild(allFamilies.get(0), "Adam", "Eva");

        assertEquals(2, familySmith.getChildren().size());
        fs.deleteAllChildrenOlderThen(3);
        assertEquals(1, familySmith.getChildren().size());
    }
    @Test
    public void testCount(){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);
        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));
        fs.createNewFamily(new Woman("Hann", "Smit", 1999), new Man("Tomi", "Smith", 1998));
        assertEquals(2, fs.count());
    }
    @Test
    public void testGetFamilyById(){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);
        fs.createNewFamily(new Woman("Hann", "Smit", 1999), new Man("Tomi", "Smith", 1998));
        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));

        Family familyById = fs.getFamilyById(1);

        assertEquals(newFamily(), familyById);

    }
    @Test
    public void testAddPet(){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);
        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));
        List<Family> allFamilies = fs.getAllFamilies();
        Pet fishDory = new Fish("Dory",1,10, new HashSet<String>());
        HashSet<Pet> pets = new HashSet<>();
        pets.add(fishDory);
        fs.addPet(0, fishDory);

        assertEquals(pets, allFamilies.get(0).getPet());
    }
    @Test
    public void testGetPets(){
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(familyDao);
        fs.createNewFamily(new Woman("Hanna", "Smith", 1999), new Man("Tom", "Smith", 1998));
        Pet fishDory = new Fish("Dory",1,10, new HashSet<String>());
        HashSet<Pet> pets = new HashSet<>();
        pets.add(fishDory);
        fs.addPet(0, fishDory);

        assertEquals(pets, fs.getPets(0));
    }

}
