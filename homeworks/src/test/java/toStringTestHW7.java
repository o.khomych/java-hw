import hw7.*;
import libs.DayOfWeek;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class toStringTestHW7 {
    public Family newFamily(){
        Human woman = new Human("Hanna", "Smith", 1999, 120, schedule());
        Human man = new Human("Tom", "Smith", 1998, 80, schedule());
        Family family = new Family(woman, man);
        return family;
    }
    public Map<DayOfWeek, String> schedule (){
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "plan");
        schedule.put(DayOfWeek.WEDNESDAY, "make a presentation");
        schedule.put(DayOfWeek.THURSDAY, "go to the jym");
        schedule.put(DayOfWeek.FRIDAY, "write an assay");
        return schedule;
    }
    @Test
    public void testPetToString() {
        Fish pet = new Fish("Dory");;
        String expected = "FISH{nickname='Dory', age=null, trickLevel=null, habits=null, FISH cannot fly, has 0 legs and hasn't fur}";
        assertEquals(expected, pet.toString());
    }
    @Test
    public void testHumanToString() {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "plan");
        Human woman = new Woman("Hanna", "Smith", 1999, 98, schedule);
        String expected = "Woman{name='Hanna', surname='Smith', year=1999, iq=98, schedule={MONDAY=plan}}";
        assertEquals(expected, woman.toString());
    }
    @Test
    public void testFamilyToString() {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "plan");
        Woman woman = new Woman("Hanna", "Smith", 1999, 98, schedule);
        Man man = new Man("Tom", "Smith", 1998, 91, schedule);
        Family family = new Family(woman, man);
        String expected = "Family{mother=Woman{name='Hanna', surname='Smith', year=1999, iq=98, schedule={MONDAY=plan}}, father=Man{name='Tom', surname='Smith', year=1998, iq=91, schedule={MONDAY=plan}}, children=[], pet=[]}";
        assertEquals(expected, family.toString());
    }

    @Test
    public void testCountFamily(){
        Family family = newFamily();
        family.bornChild();
        assertEquals(3, family.countFamily());
    }

    @Test
    public void test1DeleteChildWithIndex(){
        Family family = newFamily();
        Human child1 = family.bornChild();
        Human child2 = family.bornChild();

        ArrayList<Human> childTest = new ArrayList<>();
        childTest.add(child2);
        assertTrue(family.deleteChildIndex(0));
        assertEquals(childTest, family.getChildren());
    }
    @Test
    public void test2DeleteChildWithIndex(){
        Family family = newFamily();
        family.bornChild();

        List<Human> childTest = family.getChildren();
        assertFalse(family.deleteChildIndex(1));
        assertEquals(childTest, family.getChildren());
    }
    @Test
    public void test1DeleteChildWithObj(){
        Family family = newFamily();
        Human child1 = family.bornChild();
        Human child2 = family.bornChild();

        ArrayList<Human> childTest = new ArrayList<>();
        childTest.add(child2);
        assertTrue(family.deleteChildObj(child1));
        assertEquals(childTest, family.getChildren());
    }
    @Test
    public void test2DeleteChildWithObj(){
        Family family = newFamily();
        Human child1 = family.bornChild();
        Man child2 = new Man("Timothy", "Smith", 2021);

        List<Human>  childTest = family.getChildren();

        assertFalse(family.deleteChildObj(child2));
        assertEquals(family.getChildren(), childTest);
    }
    @Test
    public void testAddChild(){
        Family family = newFamily();

        Woman child1 = new Woman("Olive", "Smith", 2021);
        Man child2 = new Man("Timothy", "Smith", 2021);


        assertEquals(0, family.getChildren().size());

        family.addChild(child1);
        assertEquals(1, family.getChildren().size());
        assertEquals(family, child1.getFamily());

        family.addChild(child2);
        assertEquals(2, family.getChildren().size());
        Human child3 = family.bornChild();
        assertEquals(family, child3.getFamily());
    }
    @Test
    public void testEqualsPet(){
        Set<String> habits = new HashSet<String>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");
        DomesticCat cat1 = new DomesticCat("Marci",2,80,habits );
        Fish cat2 = new Fish("Marci",2,80,habits );
        DomesticCat cat3 = new DomesticCat("Marcy",2,80, habits);
        DomesticCat cat5 = new DomesticCat("Marci",2,84, habits);
        DomesticCat cat6 = new DomesticCat("Marci",6,80,habits);
        Human cat7 = new Human("Marci", "None", 1977);
        DomesticCat cat8 = new DomesticCat("Marci",2,80, habits);
        habits.add("play");
        DomesticCat cat4 = new DomesticCat("Marci",2,80, habits);
        assertFalse(cat1.equals(cat2));
        assertFalse(cat1.equals(cat3));
        assertTrue(cat1.equals(cat4));
        assertTrue(cat1.equals(cat5));
        assertFalse(cat1.equals(cat6));
        assertFalse(cat1.equals(cat7));
        assertTrue(cat1.equals(cat8));

        assertNotEquals(cat1.hashCode(), cat2.hashCode());
        assertNotEquals(cat1.hashCode(), cat3.hashCode());
        assertEquals(cat1.hashCode(), cat4.hashCode());
        assertEquals(cat1.hashCode(), cat5.hashCode());
        assertNotEquals(cat1.hashCode(), cat6.hashCode());
        assertNotEquals(cat1.hashCode(), cat7.hashCode());
        assertEquals(cat1.hashCode(), cat8.hashCode());
    }
    @Test
    public void testEqualsHuman(){
        Map<DayOfWeek, String> newSchedule = schedule();
        newSchedule.remove(DayOfWeek.MONDAY);
        Man man1 = new Man("Tom", "Swan", 1969, 98, schedule());
        Man man2 = new Man("Tom", "Swan", 1969, 98, schedule());
        Man man3 = new Man("Tomi", "Swan", 1969, 98,schedule());
        Man man4 = new Man("Tom", "Swap", 1969, 98, schedule());
        Man man5 = new Man("Tom", "Swan", 1968, 98, schedule());
        Man man6 = new Man("Tom", "Swan", 1969, 98, newSchedule);
        Man man7 = new Man("Tom", "Swan", 1969, 110, schedule());
        Woman man9 = new Woman("Tom", "Swan", 1969, 98, schedule());
        DomesticCat man8 = new DomesticCat("Marci");
        assertTrue(man1.equals(man2));
        assertFalse(man1.equals(man3));
        assertFalse(man1.equals(man4));
        assertFalse(man1.equals(man5));
        assertTrue(man1.equals(man6));
        assertTrue(man1.equals(man7));
        assertFalse(man1.equals(man8));
        assertFalse(man1.equals(man9));

        assertEquals(man1.hashCode(), man2.hashCode());
        assertNotEquals(man1.hashCode(), man3.hashCode());
        assertNotEquals(man1.hashCode(), man4.hashCode());
        assertNotEquals(man1.hashCode(), man5.hashCode());
        assertEquals(man1.hashCode(), man6.hashCode());
        assertEquals(man1.hashCode(), man7.hashCode());
        assertNotEquals(man1.hashCode(), man8.hashCode());
        assertNotEquals(man1.hashCode(), man9.hashCode());
    }
    @Test
    public void testEqualFamily(){
        Woman woman = new Woman("Hanna", "Smith", 1999, 120, schedule());
        Man man = new Man("Tom", "Smith", 1998, 80, schedule());
        Woman woman1 = new Woman("Hanna", "Smith", 2002, 120, schedule());
        Man man1 = new Man("Tom", "Smith", 2000, 80, schedule());

        Family testFamily = newFamily();
        Family family = new Family(woman1, man1);
        Family family1 = new Family(woman, man);
        family1.addChild(new Human("Olive", "Smith", 2021));

        Family family2 = new Family(woman, man);
        family2.addPet(new Fish("Dory"));

        assertTrue(testFamily.equals(new Family(woman, man)));
        assertFalse(testFamily.equals(family));
        assertFalse(testFamily.equals(new Family(woman1, man)));
        assertFalse(newFamily().equals(new Family(woman, man1)));
        assertTrue(testFamily.equals(family1));
        assertTrue(testFamily.equals(family2));


        assertNotEquals(testFamily.hashCode(), family.hashCode());
        assertNotEquals(testFamily.hashCode(), (new Family(woman1, man)).hashCode());
        assertNotEquals(testFamily.hashCode(), (new Family(woman, man1)).hashCode());
        assertEquals(testFamily.hashCode(), family1.hashCode());
        assertEquals(testFamily.hashCode(), family2.hashCode());
    }

}
