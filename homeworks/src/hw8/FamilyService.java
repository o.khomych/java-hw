package hw8;

import libs.Console;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FamilyService {
    private final FamilyDao familyDao;
    public FamilyService(FamilyDao familyDao){
        this.familyDao = familyDao;
    }
    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }
    public void displayAllFamilies(){
        for (int i = 0; i < familyDao.getAllFamilies().size(); i++) {
            Console.println(i + ": " + familyDao.getAllFamilies().get(i));
        }
    }
    public List<Family> getFamiliesBiggerThan(int familySize){
        List<Family> result = new ArrayList<>();
        for (Family family: familyDao.getAllFamilies()) {
            if (family.countFamily() > familySize){
                result.add(family);
            }
        }
        Console.println(result.toString());
        return result;
    }
    public List<Family> getFamiliesLessThan (int familySize){
        List<Family> result = new ArrayList<>();
        for (Family family: familyDao.getAllFamilies()) {
            if (family.countFamily() < familySize){
                result.add(family);
            }
        }
        Console.println(result.toString());
        return result;
    }
    public int countFamiliesWithMemberNumber (int familySize){
        List<Family> result = new ArrayList<>();
        for (Family family: familyDao.getAllFamilies()) {
            if (family.countFamily() == familySize){
                result.add(family);
            }
        }
        return result.size();
    }
    public void createNewFamily(Human woman, Human man){
        Family family = new Family(woman, man);
        familyDao.saveFamily(family);
    }

    public boolean deleteFamilyByIndex (int index){
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family  family, String boyName, String girlName){
        Human child = family.bornChild();
        if (child instanceof Man) child.setName(boyName);
        else child.setName(girlName);
        familyDao.saveFamily(family);
        return family;
    }
    public Family adoptChild (Family  family, Human child){
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }
    public void deleteAllChildrenOlderThen (int age){
        List<Family> families = familyDao.getAllFamilies();
        for (Family family: families) {
            List<Human> children = family.getChildren();
            for (int i = children.size()-1; i>=0; i--) {
                int childAge =  LocalDate.now().getYear() - children.get(i).getYear();
                if (childAge > age){
                    family.deleteChildObj(children.get(i));
                }
            }
            familyDao.saveFamily(family);
        }
    }

    public int count(){
        return familyDao.getAllFamilies().size();
    }
    public Family getFamilyById(int index){
        return familyDao.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index){
        Family family = getFamilyById(index);
        return family.getPet();
    }
    public void addPet(int index, Pet pet){
        Family family = getFamilyById(index);
        family.addPet(pet);
        getAllFamilies().set(index, family);
    }
}
