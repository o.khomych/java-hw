package hw8;

import libs.Console;
import hw7.Species;

import java.util.Set;

public class Fish extends Pet {

    public Fish(String nickname, Integer age, Integer trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.FISH;
    }
    public Fish(String nickname) {
        super(nickname);
        this.species = Species.FISH;
    }
    @Override
    public void respond() {
        Console.println("...");
    }

}
