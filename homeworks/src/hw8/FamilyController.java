package hw8;

import libs.Console;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FamilyController {
    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }
    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }
    public void displayAllFamilies(){
        familyService.displayAllFamilies();
    }
    public List<Family> getFamiliesBiggerThan(int familySize){
        return familyService.getFamiliesBiggerThan(familySize);
    }
    public List<Family> getFamiliesLessThan (int familySize){
        return familyService.getFamiliesLessThan(familySize);
    }
    public int countFamiliesWithMemberNumber (int familySize){
        return familyService.countFamiliesWithMemberNumber(familySize);
    }
    public void createNewFamily(Human woman, Human man){
        familyService.createNewFamily(woman, man);
    }
    public boolean deleteFamilyByIndex (int index){
        return familyService.deleteFamilyByIndex(index);
    }
    public Family bornChild(Family  family, String boyName, String girlName){
        return familyService.bornChild(family, boyName, girlName);
    }
    public Family adoptChild (Family  family, Human child){
        return familyService.adoptChild(family, child);
    }
    public void deleteAllChildrenOlderThen (int age){
        familyService.deleteAllChildrenOlderThen(age);
    }
    public int count(){
        return familyService.count();
    }
    public Family getFamilyById(int index){
        return familyService.getFamilyById(index);
    }
    public Set<Pet> getPets(int index){
        return familyService.getPets(index);
    }
    public void addPet(int index, Pet pet){
        familyService.addPet(index, pet);
    }
}
