package hw8;

import libs.Console;
import libs.DayOfWeek;

import java.util.Map;

public final class Woman extends Human {
    public Woman(String name, String surname, Integer year, Integer iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman(String name, String surname, Integer year) {
        super(name, surname, year);
    }

    @Override
    public void greetPet() {
        for (Pet p: getFamily().getPet()) {
            Console.println(String.format("Hi, %s.", p.getNickname()));
        }
    }

    public void makeup(){
        Console.println("Схожу в салон красоты");
    }
    @Override
    public String toString() {
        return "Woman{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }
    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null) return false;
        if (!(that instanceof Woman)) return false;
        Woman woman = (Woman) that;
        if (!this.name.equals(woman.name)) return false;
        if (!this.surname.equals(woman.surname)) return false;
        if (!this.year.equals(woman.year)) return false;
        return true;
    }
    @Override
    public int hashCode() {
        return name.hashCode()+surname.hashCode()+year.hashCode()+getClass().hashCode();
    }
}


