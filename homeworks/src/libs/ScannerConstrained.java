package libs;
import java.util.Scanner;
public class ScannerConstrained {
    private static Scanner scanner = new Scanner(System.in);
    public static String nextLine() {
               return scanner.nextLine();
    }
    public static int nextInt() {
        return scanner.nextInt();
    }
    public String next() {
        return scanner.next();
    }
}
