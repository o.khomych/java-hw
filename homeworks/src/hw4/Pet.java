package hw4;

import libs.Console;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private String species;
    private String nickname;
    private Integer age;
    private Integer trickLevel;
    private String [] habits;
    private Pet(){

    }
    public Pet (String species, String nickname, Integer age, Integer trickLevel, String[] habits){
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    public Pet(String species, String nickname){
        this(species, nickname, null, null, new String[]{});
    }
    public void eat(){
        Console.println("Я кушаю!");
    }
    public void respond(){
        Console.println(String.format("Привет, хозяин. Я - %s. Я соскучился!", nickname));
    }
    public void foul(){
        Console.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {
        return species +
                "{nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    public String getSpecies(){return species;}
    public void setSpecies(String str){species = str;}

    public String getNickname(){return nickname;}
    public void setNickname(String str){nickname = str;}

    public Integer getAge(){return age;}
    public void setAge(Integer ints){age = ints;}

    public Integer getTrickLevel(){return trickLevel;}
    public void setTrickLevel(Integer ints){
        if (ints>=0 && ints <= 100) trickLevel = ints;
    }

    public String[] getTrickLevelHabits(){return habits;}
    public void setHabits(String[] strings){habits = strings;}

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null) return false;
        if (!(that instanceof Pet)) return false;
        Pet pet = (Pet) that;
        if (!this.species.equals(pet.species)) return false;
        if (!this.nickname.equals(pet.nickname)) return false;
        if (!this.age.equals(pet.age)) return false;
        return true;
    }



    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age);
    }

    {
        Console.println("creating new object type Pet");
    }
    static {
        Console.println("new Pet class loaded");
    }

}
