package hw4;

import libs.Console;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private Integer year;
    private Integer iq;
    private String[][] schedule;
    private Family family;

    public Human (String name, String surname, Integer year, Integer iq, String[][] schedule){
      this.name=name;
      this.surname=surname;
      this.year = year;
      this.iq = iq;
      this.schedule = schedule;
    }
    public Human (String name, String surname, Integer year){
        this(name, surname, year, null, new String[][] {});
    }
    public String getName(){return name;}
    public void setName(String str){name = str;}
    public String getSurname(){return surname;}
    public void setSurname(String str){surname = str;}
    public Integer getYear(){return year;}
    public void setYear(Integer ints){year = ints;}
    public Integer getIq(){return iq;}
    public void setIq(Integer ints){iq = ints;}
    public Family getFamily(){return family;}
    public void setFamily(Family addFamily){family = addFamily;}
    public String[][] getSchedule(){return schedule;}
    public void setSchedule(String[][] strings){schedule=strings;}


    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null) return false;
        if (!(that instanceof Human)) return false;
        Human human = (Human) that;
        if (!this.name.equals(human.name)) return false;
        if (!this.surname.equals(human.surname)) return false;
        if (!this.year.equals(human.year)) return false;
//        if (!this.iq.equals(human.iq)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year);
    }

    static {
        Console.println("new Human class loaded");
    }
    {
        Console.println("creating new object type Human");
    }

    public void greetPet(){
        if(family.getPet() != null) {
            Console.println(String.format("Привет, %s.", family.getPet().getNickname()));
        } else {
            Console.println("You don't have a pet!");
        }

    }
    public void describePet(){
        if(family.getPet() != null) {
            String trickLevel = family.getPet().getTrickLevel()>=50 ? "очень хитрый" : "почти не хитрый";
            Console.println(String.format("\"У меня есть "+ family.getPet().getSpecies()+ ", " +
                    "ему "+family.getPet().getAge()+" лет, " +
                    "он "+ trickLevel + ". "));
        } else {
            Console.println("You don't have a pet!");
        }
    }

    public boolean feedPet(boolean timeToFeed){
        if(family.getPet() != null) {
            Random random = new Random();
            if (timeToFeed) {
                Console.println("Хм... покормлю ка я " + family.getPet().getNickname());
                return true;
            } else{
                int randomNumber = random.nextInt(101);
                if (family.getPet().getTrickLevel() > randomNumber) {
                    Console.println("Хм... покормлю ка я " + family.getPet().getNickname());
                    return true;
                }
                Console.println("Думаю " + family.getPet().getNickname() + " не голоден.");
                return false;
            }
        } else {
            Console.println("You don't have a pet!");
            return false;
        }

    }
}
