package hw4;

import libs.Console;


public class Main {
    public static void main(String[] args) {
        Pet catMarci = new Pet("cat", "Marci",2,80, new String[]{"eat", "drink", "sleep"});
        Human KateSwan = new Human("Kate", "Swan", 1969, 98, new String[][]{{"Monday", "plan1"},{"Friday", "plan2"}});
        Human DavidSwan = new Human("David", "Swan", 1969, 99, new String[][]{{"Monday", "plan12"},{"Wednesday", "plan22"}});
        Family Swan = new Family(KateSwan, DavidSwan);
        KateSwan.greetPet();
        Swan.setPet(catMarci);
        System.out.println(KateSwan.toString());
        System.out.println(DavidSwan.toString());


        Human AnnaNone = new Human("Anna", "None", 1977);
        Human KarlSmith = new Human("Karl", "Smith", 1976);
        Pet fishDory = new Pet("fish", "Dory");
        Family Smith = new Family(AnnaNone, KarlSmith);
        Smith.setPet(fishDory);
        System.out.println(AnnaNone.toString());
        System.out.println(KarlSmith.toString());


        Human LilyJames = new Human("Lily", "James", 1994, 124, new String[][]{{"Sunday", "plan1"},{"Monday", "plan2"}});
        Human MetthewJames = new Human("Metthew", "James", 190, 115, new String[][]{{"Monday", "plan12"},{"Tuesday", "plan22"}});
        Human OliveJames = new Human("Olive", "James", 2021);
        Human HannaJames = new Human("Hanna", "James", 2021);

        Pet dogTimi = new Pet("dog", "Timi",1,75,new String[]{"eat", "drink", "sleep", "play"});

        Family James = new Family(LilyJames, MetthewJames);
        James.setPet(dogTimi);
        Console.println(James.toString());

        James.addChild(OliveJames);
        System.out.printf("Family count: %d\n", James.countFamily());
        Console.println(OliveJames.toString());

        OliveJames.greetPet();
        OliveJames.describePet();
        OliveJames.feedPet(false);
        OliveJames.feedPet(true);
        dogTimi.eat();
        dogTimi.respond();
        dogTimi.foul();
        James.deleteChild(0);
        System.out.printf("Family count: %d\n", James.countFamily());

        James.addChild(OliveJames);
        James.addChild(HannaJames);
        James.deleteChild2(OliveJames);
        System.out.printf("Family count: %d\n", James.countFamily());
    }

}
