package hw1;
import libs.Console;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
    Random random = new Random();
    Scanner scanner = new Scanner(System.in);

    Console.println("Let the game begin!");
    int randomNumber = random.nextInt(101);
    System.out.printf("%d\n",randomNumber);
    Console.print("Please enter your name: ");
    String name = scanner.nextLine();

    int n ;
    int[] numArray = new int[0];

    do{
        Console.print("Enter your number: ");
        n = getNumber();

        if(n < randomNumber) Console.println("Your number is too small. Please, try again.");
        if(n > randomNumber) Console.println("Your number is too big. Please, try again.");

        numArray = Arrays.copyOf(numArray, numArray.length+1);
        numArray[numArray.length-1] = n;

    } while(n != randomNumber);
        Console.println("Your numbers: "+Arrays.toString(reverseSortedArray(numArray)));
        Console.println("Congratulations, "+name+"!");
    }
    public static int getNumber(){
        Scanner scanner = new Scanner(System.in);
        while(!scanner.hasNextInt()){
            Console.print("This is not a number. Please enter valid number: ");
            scanner.next();
        }
        return scanner.nextInt();
    }
    public static int[] reverseSortedArray(int[] array){
        Arrays.sort(array);
        for (int i = 0; i < array.length/2; i++) {
            int a = array[array.length - i - 1];
            array[array.length - i - 1]=array[i];
            array[i] = a;
        }
        return array;
    }
}
