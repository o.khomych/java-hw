package hw1;

import libs.Console;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class HistoryGame {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        int randomEvent = random.nextInt(101);
        String [][] data = HistoryData.Data();

        String question  = data[randomEvent][1];
        int answer = Integer.parseInt(data[randomEvent][0]);

        Console.println("Let the game begin!");
        Console.print("Please enter your name: ");
        String name = scanner.nextLine();

        int year ;
        int[] numArray = new int[0];

        System.out.printf("Коли відбулася наступна подія?\n%s'\n", question);
        do{
            Console.print("Enter your answer: ");
            year = getNumber();

            if(year < answer) Console.println("Your year is too small. Please, try again.");
            if(year > answer) Console.println("Your year is too big. Please, try again.");

            numArray = Arrays.copyOf(numArray, numArray.length+1);
            numArray[numArray.length-1] = year;

        } while(year != answer);
        Console.println("Your answers: "+Arrays.toString(reverseSortedArray(numArray)));
        Console.println("Congratulations, "+name+"!");
    }
    public static int getNumber(){
        Scanner scanner = new Scanner(System.in);
        while(!scanner.hasNextInt()){
            Console.print("This is not a number. Please enter valid year: ");
            scanner.next();
        }
        return scanner.nextInt();
    }
    public static int[] reverseSortedArray(int[] array){
        Arrays.sort(array);
        for (int i = 0; i < array.length/2; i++) {
            int a = array[array.length - i - 1];
            array[array.length - i - 1]=array[i];
            array[i] = a;
        }
        return array;
    }
}
