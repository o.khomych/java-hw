package hw3;

import libs.Console;

import java.util.Arrays;
import java.util.Scanner;

public class Planner_v2 {
    public static String checkString(String a){
        while(a.contains("  ")){
            a = a.replace("  ", " ");
        }
        return a.toLowerCase().trim();
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";

        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";

        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to the jym";

        schedule[3][0] = "Wednesday";
        schedule[3][1] = "write an assay";

        schedule[4][0] = "Thursday";
        schedule[4][1] = "go to the doctor";

        schedule[5][0] = "Friday";
        schedule[5][1] = "make a presentation";

        schedule[6][0] = "Saturday";
        schedule[6][1] = "read a book";

        boolean exit = false;

        while (!exit){
            Console.print("Please, input the day of the week: ");
            String input = scanner.nextLine();
            boolean doChange = input.contains("change") && !input.trim().equals("change");

            String inputDay = !doChange ? input.toLowerCase().trim() : checkString(input).split(" ")[1];

            String message = null;
            boolean mistake = false;

            switch (inputDay) {
                case "sunday",
                "monday",
                "tuesday",
                "wednesday",
                "thursday",
                "friday",
                "saturday" -> message = doChange ? "Please, input new tasks for %s: " : "Your tasks for %s: %s.\n";
                case "exit" -> exit = true;
                default -> {
                    message = "Sorry, I don't understand you, please try again.";
                    mistake = true;
                }
            }


            if (mistake) {
                Console.println(message);
            }else if(doChange){
                for (int i = 0; i < schedule.length; i++) {
                    if(Arrays.toString(schedule[i]).toLowerCase().contains(inputDay)) {
                        System.out.printf(message, schedule[i][0]);
                        String newTask = scanner.nextLine();
                        schedule[i][1] = newTask;
                    }
                }
                Console.println("Successfully save!");
            }else{
                for (int i = 0; i < schedule.length; i++) {
                    if((schedule[i][0]).toLowerCase().contains(inputDay)){
                        System.out.printf(message, schedule[i][0], schedule[i][1]);
                    }
                }
            }
        }
    }
}
