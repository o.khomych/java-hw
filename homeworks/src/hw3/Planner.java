package hw3;

import libs.Console;

import java.util.Scanner;

public class Planner {
    public static String checkString(String a){
        while(a.contains("  ")){
            a = a.replace("  ", " ");
        }
        return a.toLowerCase().trim();
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";

        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";

        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to the jym";

        schedule[3][0] = "Wednesday";
        schedule[3][1] = "write an assay";

        schedule[4][0] = "Thursday";
        schedule[4][1] = "go to the doctor";

        schedule[5][0] = "Friday";
        schedule[5][1] = "make a presentation";

        schedule[6][0] = "Saturday";
        schedule[6][1] = "read a book";

        boolean exit = false;

        while (!exit){
            Console.print("Please, input the day of the week: ");
            String input = scanner.nextLine();
            boolean doChange = input.contains("change") && !input.trim().equals("change");
            String inputDay = !doChange ? input.toLowerCase().trim() : checkString(input).split(" ")[1];
            String mes = doChange ? "Please, input new tasks for %s: " : "Your tasks for %s: %s.";

            String message = switch (inputDay) {
                    case "sunday" -> String.format(mes, schedule[0][0], schedule[0][1]);
                    case "monday" -> String.format(mes, schedule[1][0], schedule[1][1]);
                    case "tuesday" -> String.format(mes, schedule[2][0], schedule[2][1]);
                    case "wednesday" -> String.format(mes, schedule[3][0], schedule[3][1]);
                    case "thursday" -> String.format(mes, schedule[4][0], schedule[4][1]);
                    case "friday" -> String.format(mes, schedule[5][0], schedule[5][1]);
                    case "saturday" -> String.format(mes, schedule[6][0], schedule[6][1]);
                    case "exit" -> null;
                    default -> "Sorry, I don't understand you, please try again.";
                };


            if (input.contains("exit")) {
                exit = true;
            } else{
                Console.println(message);
            }

            if(doChange){
                String newTask = scanner.nextLine();
                switch (inputDay) {
                    case "sunday" -> schedule[0][1] = newTask;
                    case "monday" -> schedule[1][1] = newTask;
                    case "tuesday" -> schedule[2][1] = newTask;
                    case "wednesday" -> schedule[3][1] = newTask;
                    case "thursday" -> schedule[4][1] = newTask;
                    case "friday" -> schedule[5][1] = newTask;
                    case "saturday" -> schedule[6][1] = newTask;
                }
                Console.println("Successfully save!");
            }
        }
    }
}
