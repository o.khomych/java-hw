package hw12.command;

import hw12.dao.FamilyController;
import hw12.entity.Family;
import java.util.List;


public class SaveData {
    public static void saveData(FamilyController FC){
        List<Family> f = FC.getAllFamilies();
        FC.loadData(f);
    }
}
