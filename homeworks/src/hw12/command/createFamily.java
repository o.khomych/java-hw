package hw12.command;

import hw12.dao.FamilyController;
import hw12.entity.Human;
import hw12.entity.Man;
import hw12.entity.Woman;
import libs.Console;
import libs.ScannerConstrained;

import static hw11.command.Validation.validateIQ;
import static hw11.command.Validation.validateString;
import static hw11.command.checkBD.*;

public class createFamily {
    public static void createFamily(FamilyController FC){
        FC.createNewFamily(createParent("mother"), createParent("father"));
    }
    public static Human createParent(String parent){
        String P = parent.equals("mother") ? "матері" : "батька";
        Console.println("Введіть наступні дані ");
        Console.print("Імя "+P+": ");
        String name = validateString(ScannerConstrained.nextLine());
        Console.print("Прізвище " + P + ": ");
        String surname = validateString(ScannerConstrained.nextLine());
        Console.print("Рік народження " + P + ": ");
        String BY = validateBY(ScannerConstrained.nextLine());
        Console.print("Місяць народження " + P + ": ");
        String BM = validateBM(ScannerConstrained.nextLine());
        Console.print("День народження "+P+": ");
        String BD = validateBD(ScannerConstrained.nextLine(), BM);
        Console.print("IQ "+P+": ");
        String IQ = validateIQ(ScannerConstrained.nextLine());
        return parent.equals("mother") ? new Woman(name, surname, String.format("%s/%s/%s", BD, BM, BY),  Integer.parseInt(IQ))
                : new Man(name, surname, String.format("%s/%s/%s", BD, BM, BY),  Integer.parseInt(IQ));

    }

}
