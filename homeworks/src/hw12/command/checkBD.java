package hw12.command;


import libs.Console;
import libs.ScannerConstrained;

import static hw11.command.Validation.isInteger;

public class checkBD {

    public static String validateBY(String BY){
        while (!isInteger(BY) || Integer.parseInt(BY) < 1923 || Integer.parseInt(BY) > 2005){
            Console.print("Ви ввели не коректний рік.\nВведіть нове значення: ");
            BY = ScannerConstrained.nextLine();
        }
        return BY;
    }
    public static String validateBM(String BM){
        while (!isInteger(BM) || Integer.parseInt(BM) < 1 || Integer.parseInt(BM) > 12){
            Console.print("Ви ввели не коректне число.\nВведіть нове значення(01-12): ");
            BM = ScannerConstrained.nextLine();
        }
        BM = Integer.parseInt(BM) < 10 ? String.format("0%s", Integer.parseInt(BM)) : BM;
        return BM;
    }
    public static String validateBD(String BD, String BM){
        boolean isValide = isInteger(BD) && validateDateVsMonth(BD, BM);
        while (!isValide){
            Console.print("Ви ввели не коректне дату.\nВведіть нове значення: ");
            BD = ScannerConstrained.nextLine();
            boolean isInt = isInteger(BD);
            isValide = isInteger(BD) && validateDateVsMonth(BD, BM);
        }
        BD = Integer.parseInt(BD) < 10 ? String.format("0%s", Integer.parseInt(BD)) : BD;
        return BD;
    }
    private static boolean validateDateVsMonth(String BD, String BM){
        if(Integer.parseInt(BD)<1){return false;}
        if (Integer.parseInt(BD) >= 29 && Integer.parseInt(BM) == 2){
            Console.print("[01-28]: ");
            return false;}
        if(Integer.parseInt(BD) >= 31 && (Integer.parseInt(BM) == 4
                || Integer.parseInt(BM) == 6 || Integer.parseInt(BM) == 9 || Integer.parseInt(BM) == 11)){
            Console.print("[01-30]: ");
            return false;
        }
        if((Integer.parseInt(BD) >= 32 && (Integer.parseInt(BM) == 1
                || Integer.parseInt(BM) == 3 || Integer.parseInt(BM) == 5
                || Integer.parseInt(BM) == 8 || Integer.parseInt(BM) == 10
                || Integer.parseInt(BM) == 12))){
            Console.print("[01-31]: ");
            return false;
        }
        return true;
    }
}
