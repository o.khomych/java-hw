package hw12.command;

import hw12.dao.FamilyController;
import libs.Console;

import static hw12.command.Validation.userNumber;

public class familyInfo {
    public static void familyBiggerThan(FamilyController FC){
        Console.print("Введіть число: ");
        int num = userNumber();
        while (num < 1){
            Console.print("Сімя не можу мати менше 2 членів родити. Введіть число: ");
            num = userNumber();
        }
        FC.getFamiliesBiggerThan(num);
    }
    public static void familyLessThan(FamilyController FC){
        Console.print("Введіть число: ");
        int num = userNumber();
        while (num < 1){
            Console.print("Сімя не можу мати менше 2 членів родити. Введіть число: ");
            num = userNumber();
        }
        FC.getFamiliesLessThan(num);
    }
    public static void countFamily(FamilyController FC){
        Console.print("Введіть число: ");
        int num = userNumber();
        while (num < 2){
            Console.print("Сімя не можу мати менше 2 членів родити. Введіть число: ");
            num = userNumber();
        }
        System.out.printf("Кількість сімей: %d", FC.countFamiliesWithMemberNumber(num));
    }
    public static void deleteChildren(FamilyController FC){
        Console.print("Введіть вік дітей: ");
        int num = userNumber();
        while (num < 0){
            Console.print("Вік дитини не може бути менше 0. Введіть число: ");
            num = userNumber();
        }
        FC.deleteAllChildrenOlderThen(num);
        Console.print("Успішно видалено!");
    }
    public static void deleteFamily(FamilyController FC){
        Console.print("Введіть порядковий номер сім'ї: ");
        int index = userNumber() - 1;
        if(!FC.deleteFamilyByIndex(index)) {
            Console.print("Такої сім'ї не існує.");
        } else {
            Console.print("Успішно видалено!");
        };
    }
}