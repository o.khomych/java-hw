package hw12.command;

import libs.Console;
import libs.ScannerConstrained;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class Validation {
    public static int userNumber(){
        String number = ScannerConstrained.nextLine();
        while (!isInteger(number)){
            Console.print("Ви ввели не коректне число.\nВведіть нове значення: ");
            number = ScannerConstrained.nextLine();
        }
        return Integer.parseInt(number);
    }
    public static boolean isInteger(String s){
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException ex){
            return false;
        } catch (NullPointerException ex){
            return false;
        }
        return true;
    }
    public static String validateIQ(String IQ){
        while (!isInteger(IQ) || Integer.parseInt(IQ) <0 || Integer.parseInt(IQ) > 200){
            Console.print("Ви ввели не коректне число.\nВведіть нове значення (0-200): ");
            IQ = ScannerConstrained.nextLine();
        }
        return IQ;
    }
    public static String validateString(String s){
        while (s.equals("") || isInteger(s)){
            if(isInteger(s)) Console.print("Ви ввели число.\nВведіть нове значення: ");
            else Console.print("Ви ввели пустий рядок.\nВведіть нове значення: ");
            s = ScannerConstrained.nextLine();
        }
        return s;
    }
    public static int validateIndex(String i){
        while (!isInteger(i) || Integer.parseInt(i) <0){
            Console.print("Ви ввели не коректне число.\nВведіть нове значення (>0): ");
            i = ScannerConstrained.nextLine();
        }
        return Integer.parseInt(i);
    }
    public static boolean validateDate(String s){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        try {
            LocalDate date = LocalDate.parse(s, formatter);
            return true;
        } catch (DateTimeParseException ex) {
            System.out.println("Некоретна дата народження. Необхідний формат \"dd/MM/yyyy\"");
            return false;
        }
    }
    public static String bDay(){
        String date = ScannerConstrained.nextLine();
        while (!validateDate(date)){
            Console.print("Ви ввели не коректне число.\nВведіть нове значення: ");
            date = ScannerConstrained.nextLine();
        }
        return date;
    }
}
