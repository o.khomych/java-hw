package hw12.command;

import libs.Console;

public class Menu {
    public static void mainMenu(){
        Console.println("""
                
                -----------------------------------------------------------------
                                           Головне меню
                -----------------------------------------------------------------
                1. Завантажити дані
                2. Отобразить весь список семей
                3. Отобразить список семей, где количество людей больше заданного
                4. Отобразить список семей, где количество людей меньше заданного
                5. Подсчитать количество семей, где количество членов равно
                6. Создать новую семью
                7. Удалить семью по индексу семьи в общем списке
                8. Редактировать семью по индексу семьи в общем списке
                9. Удалить всех детей старше возраста
                10. Зберегти зміни
                Exit
                -----------------------------------------------------------------
                """);
        Console.print("Виберіть команду: ");
    }
    public static void editFamilyMenu(){
        Console.println("""
                ---------------------------
                    Оберіть пункт меню:
                ---------------------------
                1. Родить ребенка
                2. Усыновить ребенка
                3. Вернуться в главное меню         
                ---------------------------
                """);
        Console.print("Виберіть команду: ");
    }
}
