package hw12;

import hw12.command.Menu;
import hw12.dao.CollectionFamilyDao;
import hw12.dao.FamilyController;
import hw12.dao.FamilyDao;
import hw12.dao.FamilyService;
import libs.Console;
import libs.ScannerConstrained;

import static hw12.command.EditFamily.editFamily;
import static hw12.command.SaveData.saveData;
import static hw12.command.createFamily.createFamily;
import static hw12.command.familyInfo.*;
import static hw12.command.loadData.load;

public class Main {
    public static void main(String[] args) {
       FamilyDao familyDao = new CollectionFamilyDao();
       FamilyService familyService = new FamilyService(familyDao);
       FamilyController FC = new FamilyController(familyService);

       while(true){
           Menu.mainMenu();
           String i = ScannerConstrained.nextLine().toLowerCase();
           switch (i){
               case "1" -> load(FC);
               case "2" -> FC.displayAllFamilies();
               case "3" -> familyBiggerThan(FC);
               case "4" -> familyLessThan(FC);
               case "5" -> countFamily(FC);
               case "6" -> createFamily(FC);
               case "7" -> deleteFamily(FC);
               case "8" -> editFamily(FC);
               case "9" -> deleteChildren(FC);
               case "10" -> saveData(FC);
               case "exit" -> System.exit(0);
               default -> Console.print("Такого пункту меню не існує, спробуйте ще раз.");
           }
       }

    }
}
