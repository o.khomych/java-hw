package hw12.dao;

import hw12.FamilyOverflowException;
import hw12.entity.Family;
import hw12.entity.Human;
import hw12.entity.Pet;

import java.util.List;
import java.util.Set;

public class FamilyController {
    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }
    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }
    public void displayAllFamilies(){
        familyService.displayAllFamilies();
    }
    public List<Family> getFamiliesBiggerThan(int familySize){
        return familyService.getFamiliesBiggerThan(familySize);
    }
    public List<Family> getFamiliesLessThan (int familySize){
        return familyService.getFamiliesLessThan(familySize);
    }
    public int countFamiliesWithMemberNumber (int familySize){
        return familyService.countFamiliesWithMemberNumber(familySize);
    }
    public void createNewFamily(Human woman, Human man){
        familyService.createNewFamily(woman, man);
    }
    public boolean deleteFamilyByIndex (int index){
        return familyService.deleteFamilyByIndex(index);
    }
    public Family bornChild(Family family, String boyName, String girlName){
            if (family.countFamily() >= 5){
                throw new FamilyOverflowException("Дитину не створено. Розмір сім'ї перевищує допустимий!");
            } else {
                return familyService.bornChild(family, boyName, girlName);
            }

    }
    public Family adoptChild (Family family, Human child){
        if (family.countFamily() > 5){
            throw new FamilyOverflowException("Розмір сім'ї перевищує допустимий");
        }
        return familyService.adoptChild(family, child);
    }
    public void deleteAllChildrenOlderThen (int age){
        familyService.deleteAllChildrenOlderThen(age);
    }
    public int count(){
        return familyService.count();
    }
    public Family getFamilyById(int index){
        return familyService.getFamilyById(index);
    }
    public Set<Pet> getPets(int index){
        return familyService.getPets(index);
    }
    public void addPet(int index, Pet pet){
        familyService.addPet(index, pet);
    }

    public void loadData(List<Family> families) {
        familyService.loadData(families);
    }
    public void loadFromDB(){
        familyService.loadFromDB();
    }
}
