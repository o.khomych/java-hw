package hw12.dao;

import hw12.entity.*;
import libs.Console;
import libs.DayOfWeek;

import java.io.*;
import java.util.*;

public class Database {
    private static Database DB;
    private final File file = new File("./families.txt");
    private Database(){};
    public static Database getDB(){
        if(DB == null) DB = new Database();
        return DB;
    }
    public void create(){
        try{
            boolean success = file.createNewFile();
            if (success) {
                System.out.println("File created successfully!");
                try (FileOutputStream fos = new FileOutputStream(file);
                     ObjectOutputStream oos = new ObjectOutputStream(fos)){
                     oos.writeObject(creatingTestDB());
                } catch (IOException ex) {
                    Console.println("Error saving data: " + ex.getMessage());
                }
            } else {
                System.out.println("File already exists.");
            }
        }catch (IOException ex){
            System.out.println("An error occurred: " + ex.getMessage());
        }
    }
    public List<Family> load(){
        if(!file.exists()) {
            create();
            save(creatingTestDB());
        }
        List<Family> families;
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(file))) {
            families = (ArrayList<Family>) in.readObject();
            if(families == null){
                families = new ArrayList<>();
            }
            Console.println("Дані успішно завантажені!");
            return families;
        } catch (IOException | ClassNotFoundException ex) {
            Console.println("Error loading data: " + ex.getMessage());
            return new ArrayList<>();
        }
    }
    public boolean save(List<Family> f){
        try (FileOutputStream fos = new FileOutputStream(file);
             ObjectOutputStream oos = new ObjectOutputStream(fos)){
            oos.writeObject(f);
            Console.println("Дані успішно зебережено!");
            return true;
        } catch (IOException ex) {
            Console.println("Error saving data: " + ex.getMessage());
            return false;
        }
    }
    public List<Family> creatingTestDB(){
        List<Family> families = new ArrayList<>();
        Set<String> habits = new HashSet<String>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");
        Set<String> fishHabits = new HashSet<String>();
        fishHabits.add("eat");
        fishHabits.add("sleep");
        fishHabits.add("swim");

        Pet catMarci = new DomesticCat("Marci",2,80, habits);
        Pet fishDory = new Fish("Dory",1,10, fishHabits);

        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "do home work");
        schedule.put(DayOfWeek.FRIDAY, "write an assay");

        Woman LilyEvans = new Woman("Lily", "Evans", "12/01/1994", 124, schedule);
        Man MetthewJames = new Man("Metthew", "James", "12/01/1990", 115, schedule);
        Family James = new Family(LilyEvans, MetthewJames);
        James.addChild(new Woman("Hanna", "James", "12/01/2021", 114, new HashMap<>()));

        Woman KateNoty = new Woman("Kate", "Noty", "12/01/1993", 134, schedule);
        Man BobySmith = new Man("Boby", "Smith", "12/01/1980", 113, schedule);
        Family Smith = new Family(KateNoty, BobySmith);
        Smith.addPet(fishDory);

        Woman CaraWilly = new Woman("Cara", "Willy", "12/01/1983", 112, schedule);
        Man BillMilligan = new Man("Bill", "Milligan", "12/01/1989", 121, schedule);
        Family Milligan = new Family(CaraWilly, BillMilligan);

        Woman SaraConor = new Woman("Sara", "Conor", "12/01/1987", 109, schedule);
        Man FillWeber = new Man("Fill", "Werb", "12/01/1987", 109, schedule);
        Family Weber = new Family(SaraConor, FillWeber);
        Weber.addPet(catMarci);
        Weber.addPet(fishDory);
        Weber.bornChild();

        Woman AnnDary = new Woman("Ann", "Dary", "12/01/1986", 90, schedule);
        Man DanTolos = new Man("Dan", "Tolos", "12/01/1986", 89, schedule);
        Family Tolos = new Family(AnnDary, DanTolos);
        Tolos.addChild(new Man("Devid", "Tolos", "25/11/2008", 88, new HashMap<>()));
        Tolos.bornChild();
        Tolos.bornChild();

        families.add(James);
        families.add(Smith);
        families.add(Milligan);
        families.add(Weber);
        families.add(Tolos);
        return families;
    }

}
