package hw12.entity;

import libs.Console;
import libs.DayOfWeek;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class Human implements Serializable {
    protected String name;
    protected String surname;
    protected long birthDate ;
    protected Integer iq;
    protected Map<DayOfWeek, String> schedule;
    protected Family family;
    private Human(){}
    public Human(String name, String surname, String birthDateString, Integer iq, Map<DayOfWeek, String> schedule){
        this.name=name;
        this.surname=surname;
        this.iq = iq;
        this.schedule = schedule;
        this.birthDate = DataConvert(birthDateString);
    }
    public Human(String name, String surname, String birthDateString){
        this(name, surname, birthDateString, null, null);
    }
    public Human(String name, String surname, String birthDateString, Integer iq){
       this(name, surname, birthDateString, iq, null);
    }

    private long DataConvert(String line){
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.parse(line, format);
        return localDate.toEpochDay();
    }

    public String getName(){return name;}
    public void setName(String str){name = str;}
    public String getSurname(){return surname;}
    public void setSurname(String str){surname = str;}
    public long getYear(){return birthDate;}
    public void setYear(long date){birthDate = date;}
    public Integer getIq(){return iq;}
    public void setIq(Integer ints){iq = ints;}
    public Family getFamily(){return family;}
    public void setFamily(Family addFamily){family = addFamily;}
    public Map<DayOfWeek, String> getSchedule(){return schedule;}
    public void setSchedule(Map<DayOfWeek, String> strings){schedule=strings;}


    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthday=" + LocalDate.ofEpochDay(birthDate).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null) return false;
        if (!(that instanceof Human)) return false;
        Human human = (Human) that;
        if (!this.name.equals(human.name)) return false;
        if (!this.surname.equals(human.surname)) return false;
        if (this.birthDate != human.birthDate) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate);
    }

    public void greetPet(){
        for (Pet p: family.getPet()) {
            Console.println(String.format("Привет, %s.", p.getNickname()));
        }
    }
    public void describePet(){
        for (Pet p: family.getPet()) {
        String trickLevel = p.getTrickLevel()>=50 ? "очень хитрый" : "почти не хитрый";
        Console.println(String.format("У меня есть "+ p.getSpecies()+ ", " +
                "ему "+p.getAge()+" лет, " +
                "он "+ trickLevel + ". "));
        }
    }

    public boolean feedPet(boolean timeToFeed, Pet pet){
        if(!family.getPet().contains(pet)){
            Console.println("Это не ваш питомец");
            return false;
        };
        Random random = new Random();
        if (timeToFeed) {
            Console.println("Хм... покормлю ка я " + pet.getNickname());
            return true;
        } else{
            int randomNumber = random.nextInt(101);
            if (pet.getTrickLevel() > randomNumber) {
                Console.println("Хм... покормлю ка я " + pet.getNickname());
                return true;
            }
            Console.println("Думаю " + pet.getNickname() + " не голоден.");
            return false;
        }
    }
    public String describeAge(){
        LocalDate today = LocalDate.now();
        LocalDate birthDay = LocalDate.ofEpochDay(birthDate);
        Period period = Period.between(birthDay, today);
        return String.format("%s years, %s months та %s days", period.getYears(), period.getMonths(), period.getDays());
    }

    public String prettyFormat(){

        return "{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", " + birthdayToString(birthDate) +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }
    private String birthdayToString(long BD){
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return "birthday="+LocalDate.ofEpochDay(BD).format(format);
    }
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Object: " +this+" is deleted");
    }

}
