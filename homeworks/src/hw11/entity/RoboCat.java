package hw11.entity;

import hw7.Species;
import libs.Console;

import java.util.Set;

public class RoboCat extends Pet {

    public RoboCat(String nickname, Integer age, Integer trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.RoboCat;
    }

    public RoboCat(String nickname) {
        super(nickname);
        this.species = Species.RoboCat;
    }

    @Override
    public void respond() {
        Console.println("I am robot cat, my name is " + getNickname());
    }
}
