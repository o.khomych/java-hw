package hw11.entity;

import hw7.Species;
import libs.Console;

import java.util.Set;

public class Fish extends Pet {

    public Fish(String nickname, Integer age, Integer trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.FISH;
    }
    public Fish(String nickname) {
        super(nickname);
        this.species = Species.FISH;
    }
    @Override
    public void respond() {
        Console.println("...");
    }

}
