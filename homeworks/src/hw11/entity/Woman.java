package hw11.entity;

import libs.Console;
import libs.DayOfWeek;

import java.util.Map;

public final class Woman extends Human {
    public Woman(String name, String surname, String birthDateString, Integer iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthDateString, iq, schedule);
    }
    public Woman(String name, String surname, String birthDateString) {
        super(name, surname, birthDateString);
    }
    public Woman(String name, String surname, String birthDateString, Integer iq) {
        super(name, surname, birthDateString, iq);
    }
    @Override
    public void greetPet() {
        for (Pet p: getFamily().getPet()) {
            Console.println(String.format("Hi, %s.", p.getNickname()));
        }
    }

    public void makeup(){
        Console.println("Схожу в салон красоты");
    }

    @Override
    public String toString() {
        return "Woman"+super.toString();
    }
    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null) return false;
        if (!(that instanceof Woman)) return false;
        Woman woman = (Woman) that;
        if (!this.name.equals(woman.name)) return false;
        if (!this.surname.equals(woman.surname)) return false;
        if (this.birthDate != woman.birthDate) return false;
        return true;
    }
    @Override
    public int hashCode() {
        return name.hashCode()+surname.hashCode()+ (int)birthDate +getClass().hashCode();
    }
}
