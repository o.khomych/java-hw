package hw11.command;

import hw11.entity.Family;
import hw11.FamilyOverflowException;
import hw11.entity.Human;
import hw11.dao.FamilyController;
import libs.Console;
import libs.ScannerConstrained;


import static hw11.command.Validation.*;

public class EditFamily {

    public static void editFamily(FamilyController fc){
        Menu.editFamilyMenu();
        switch(String.valueOf(userNumber()).toLowerCase()){
            case "1" -> bornChild(fc);
            case "2" -> adoptChild(fc);
            case "3" -> Menu.mainMenu();
            case "exit" -> System.exit(0);
            default -> {
                ScannerConstrained.nextInt();;

            }
        };
    };
    private static void bornChild(FamilyController fc){
        Console.print("Введіть порядковий номер сім'ї: ");
        int index = validateIndex(ScannerConstrained.nextLine());
        Console.print("Введіть ім'я для хлопчика: ");
        String boyName = validateString(ScannerConstrained.nextLine());
        Console.print("Введіть ім'я для дівчинки: ");
        String girlName = validateString(ScannerConstrained.nextLine());
        Family family;
        try{
            family = fc.getFamilyById(index-1);
            try{
                Family updateFamily = fc.bornChild(family, boyName, girlName);
                Console.println("Дитину додано!");
                Console.println(updateFamily.prettyFormat());
            } catch(FamilyOverflowException ex){
                Console.println(ex.getMessage());
            }
        } catch(NullPointerException ex){
            Console.println("Такої сім'ї в базі не існує!");
        }
    }

    private static void adoptChild(FamilyController fc){
        Console.print("Введіть порядковий номер сім'ї: ");
        int index = validateIndex(ScannerConstrained.nextLine());
        Console.print("Введіть ім'я: ");
        String name = validateString(ScannerConstrained.nextLine());
        Console.print("Введіть прізвище: ");
        String surname = validateString(ScannerConstrained.nextLine());
        Console.print("Введіть рік народження у форматі dd/MM/yyyy: ");
        String birthDay = bDay();
        Console.print("Введіть IQ: ");
        String iq = validateIQ(ScannerConstrained.nextLine());

        Family family;
        try{
            family = fc.getFamilyById(index-1);
            try{
                Family updateFamily = fc.adoptChild(family, new Human(name, surname, birthDay, Integer.parseInt(iq)));
                Console.println("Дитину додано");
                Console.println(updateFamily.prettyFormat());
            } catch(FamilyOverflowException ex){
                Console.println(ex.getMessage());
            }
        } catch(NullPointerException ex){
            Console.println("Такої сім'ї в базі не існує");
        }
    }
}
