package hw11.command;

import hw11.entity.Human;
import hw11.entity.Man;
import hw11.entity.Woman;
import libs.Console;
import libs.ScannerConstrained;

import static hw11.command.Validation.*;
import static hw11.command.checkBD.*;

public class createHuman {
    public static Human createParent(String parent){
        String P = parent.equals("mother") ? "матері" : "батька";
        Console.println("Введіть наступні дані ");
        Console.print("Імя "+P+": ");
        String name = validateString(ScannerConstrained.nextLine());
        Console.print("Прізвище " + P + ": ");
        String surname = validateString(ScannerConstrained.nextLine());
        Console.print("Рік народження " + P + ": ");
        String BY = validateBY(ScannerConstrained.nextLine());
        Console.print("Місяць народження " + P + ": ");
        String BM = validateBM(ScannerConstrained.nextLine());
        Console.print("День народження "+P+": ");
        String BD = validateBD(ScannerConstrained.nextLine(), BM);
        Console.print("IQ "+P+": ");
        String IQ = validateIQ(ScannerConstrained.nextLine());
        return parent.equals("mother") ? new Woman(name, surname, String.format("%s/%s/%s", BD, BM, BY),  Integer.parseInt(IQ))
                : new Man(name, surname, String.format("%s/%s/%s", BD, BM, BY),  Integer.parseInt(IQ));

    }

}
