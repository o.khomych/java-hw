package hw11.command;

import hw11.dao.CollectionFamilyDao;
import hw11.dao.FamilyController;
import hw11.dao.FamilyDao;
import hw11.dao.FamilyService;
import hw11.entity.*;
import libs.Console;
import libs.DayOfWeek;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Database {
    FamilyDao familyDao = new CollectionFamilyDao();
    FamilyService familyService = new FamilyService(familyDao);
    FamilyController FC = new FamilyController(familyService);
    public static FamilyDao creatingTestDB(){
        FamilyDao DB = new CollectionFamilyDao();
        Set<String> habits = new HashSet<String>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");
        Set<String> fishHabits = new HashSet<String>();
        fishHabits.add("eat");
        fishHabits.add("sleep");
        fishHabits.add("swim");

        Pet catMarci = new DomesticCat("Marci",2,80, habits);
        Pet fishDory = new Fish("Dory",1,10, fishHabits);

        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "do home work");
        schedule.put(DayOfWeek.FRIDAY, "write an assay");

        Woman LilyEvans = new Woman("Lily", "Evans", "12/01/1994", 124, schedule);
        Man MetthewJames = new Man("Metthew", "James", "12/01/1990", 115, schedule);
        Family James = new Family(LilyEvans, MetthewJames);
        James.addChild(new Woman("Hanna", "James", "12/01/2021", 114, new HashMap<>()));

        Woman KateNoty = new Woman("Kate", "Noty", "12/01/1993", 134, schedule);
        Man BobySmith = new Man("Boby", "Smith", "12/01/1980", 113, schedule);
        Family Smith = new Family(KateNoty, BobySmith);
        Smith.addPet(fishDory);

        Woman CaraWilly = new Woman("Cara", "Willy", "12/01/1983", 112, schedule);
        Man BillMilligan = new Man("Bill", "Milligan", "12/01/1989", 121, schedule);
        Family Milligan = new Family(CaraWilly, BillMilligan);

        Woman SaraConor = new Woman("Sara", "Conor", "12/01/1987", 109, schedule);
        Man FillWeber = new Man("Fill", "Werb", "12/01/1987", 109, schedule);
        Family Weber = new Family(SaraConor, FillWeber);
        Weber.addPet(catMarci);
        Weber.addPet(fishDory);
        Weber.bornChild();

        Woman AnnDary = new Woman("Ann", "Dary", "12/01/1986", 90, schedule);
        Man DanTolos = new Man("Dan", "Tolos", "12/01/1986", 89, schedule);
        Family Tolos = new Family(AnnDary, DanTolos);
        Tolos.addChild(new Man("Devid", "Tolos", "25/11/2008", 88, new HashMap<>()));
        Tolos.bornChild();
        Tolos.bornChild();

        DB.saveFamily(James);
        DB.saveFamily(Smith);
        DB.saveFamily(Milligan);
        DB.saveFamily(Weber);
        DB.saveFamily(Tolos);

        Console.println("Дані заповнено!");
        return DB;
    }
}
