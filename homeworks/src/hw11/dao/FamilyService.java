package hw11.dao;

import hw11.entity.Family;
import hw11.entity.Human;
import hw11.entity.Man;
import hw11.entity.Pet;
import libs.Console;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    private final FamilyDao familyDao;
    public FamilyService(FamilyDao familyDao){
        this.familyDao = familyDao;
    }
    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }
    public void displayAllFamilies(){
        familyDao.getAllFamilies()
                .stream()
                .forEach(family -> System.out.println((getAllFamilies().indexOf(family)+1) + ": "+ family.prettyFormat()));
        // or
//        IntStream
//                .range(0, getAllFamilies().size())
//                .forEach(i -> System.out.println(i + ": "+ getAllFamilies().get(i)));
    }
    public List<Family> getFamiliesBiggerThan(int familySize){
        List<Family> result =  familyDao.getAllFamilies()
                .stream()
                .filter(x -> x.countFamily() > familySize)
                .collect(Collectors.toList());;
        result.stream().forEach(x -> Console.println((result.indexOf(x)+1) + ": "+ x.prettyFormat()));
        return result;
    }
    public List<Family> getFamiliesLessThan (int familySize){
        List<Family> result = familyDao.getAllFamilies()
                .stream()
                .filter(x -> x.countFamily() < familySize)
                .collect(Collectors.toList());
        result.stream().forEach(x -> Console.println((result.indexOf(x)+1) + ": "+ x.prettyFormat()));
        return result;
    }
    public int countFamiliesWithMemberNumber (int familySize){
        return (int)familyDao.getAllFamilies()
                .stream()
                .filter(x -> x.countFamily() == familySize)
                .count();
    }
    public void createNewFamily(Human woman, Human man){
        Family family = new Family(woman, man);
        familyDao.saveFamily(family);
    }

    public boolean deleteFamilyByIndex (int index){
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String boyName, String girlName){
        Human child = family.bornChild();
        if (child instanceof Man) child.setName(boyName);
        else child.setName(girlName);
        familyDao.saveFamily(family);
        return family;
    }
    public Family adoptChild (Family family, Human child){
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }
    public void deleteAllChildrenOlderThen (int age){
        familyDao.getAllFamilies()
                .stream()
                .forEach(family -> {
                    List<Human> childrenToDelete =
                            family.getChildren()
                            .stream()
                            .filter(child -> Period.between(LocalDate.ofEpochDay(child.getYear()), LocalDate.now()).getYears() > age)
                            .collect(Collectors.toList());
                    childrenToDelete.forEach(family::deleteChildObj);
                    familyDao.saveFamily(family);
                });
    }

    public int count(){
        return familyDao.getAllFamilies().size();
    }
    public Family getFamilyById(int index){
        return familyDao.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index){
        Family family = getFamilyById(index);
        return family.getPet();
    }
    public void addPet(int index, Pet pet){
        Family family = getFamilyById(index);
        family.addPet(pet);
        getAllFamilies().set(index, family);
    }
}
