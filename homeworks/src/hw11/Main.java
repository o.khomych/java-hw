package hw11;

import hw11.command.Menu;
import hw11.command.checkBD;
import hw11.dao.FamilyController;
import hw11.dao.FamilyDao;
import hw11.dao.FamilyService;
import hw11.dao.CollectionFamilyDao;
import hw11.entity.*;
import libs.Console;
import libs.DayOfWeek;
import libs.ScannerConstrained;

import java.util.*;

import static hw11.command.Database.creatingTestDB;
import static hw11.command.EditFamily.editFamily;
import static hw11.command.createHuman.createParent;
import static hw11.command.familyInfo.*;

public class Main {
    public static void main(String[] args) {
       FamilyDao familyDao = new CollectionFamilyDao();
       FamilyService familyService = new FamilyService(familyDao);
       FamilyController FC = new FamilyController(familyService);

       while(true){
           Menu.mainMenu();
           String i = ScannerConstrained.nextLine().toLowerCase();
           switch (i){
               case "1" -> FC = new FamilyController(new FamilyService(creatingTestDB()));
               case "2" -> FC.displayAllFamilies();
               case "3" -> familyBiggerThan(FC);
               case "4" -> familyLessThan(FC);
               case "5" -> countFamily(FC);
               case "6" -> FC.createNewFamily(createParent("mother"), createParent("father"));
               case "7" -> deleteFamily(FC);
               case "8" -> editFamily(FC);
               case "9" -> deleteChildren(FC);
               case "exit" -> System.exit(0);
               default -> Console.print("Такого пункту меню не існує, спробуйте ще раз.");
           }
       }

    }
}
