package hw10;

public interface HumanCreator {
    String[] availableWomanNames = new String[]{"Hanna", "Mery", "Dina", "Elithabeth", "Jane", "Kity", "Sharlot"};
    String[] availableManNames = new String[]{"Alex", "Dima", "Sasha", "Mike", "Slava", "Henry", "Kile"};
    public Human bornChild();
}
