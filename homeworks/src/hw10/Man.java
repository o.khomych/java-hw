package hw10;

import libs.Console;
import libs.DayOfWeek;

import java.util.Map;

public final class Man extends Human {

    public Man(String name, String surname, String birthDateString, Integer iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthDateString, iq, schedule);
    }
    public Man(String name, String surname, String birthDateString) {
        super(name, surname, birthDateString);
    }

    public Man(String name, String surname, String birthDateString, Integer iq) {
        super(name, surname, birthDateString, iq);
    }
    @Override
    public void greetPet() {
        for (Pet p: getFamily().getPet()) {
            Console.println(String.format("Привет, %s. Я купил тебе корм.", p.getNickname()));
        }
    }
    public void repairCar(){
        Console.println("Отвезу машину в ремонт!");
    }
    @Override
    public String toString() {
        return "Man" + super.toString();
    }
    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null) return false;
        if (!(that instanceof Man)) return false;
        Man man = (Man) that;
        if (!this.name.equals(man.name)) return false;
        if (!this.surname.equals(man.surname)) return false;
        if (this.birthDate != man.birthDate) return false;
        return true;
    }
    @Override
    public int hashCode() {
        return name.hashCode()+surname.hashCode()+(int)birthDate +getClass().hashCode();
    }
}
