package hw10;

import hw7.Species;
import libs.Console;

import java.util.Objects;
import java.util.Set;

public abstract class Pet {
    protected Species species;
    protected String nickname;
    private Integer age;
    private Integer trickLevel;
    private Set<String> habits;
    private Pet(){

    }
    public Pet (String nickname, Integer age, Integer trickLevel, Set<String> habits){
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.species = Species.UNKNOWN;
    }
    public Pet(String nickname){
        this(nickname, null, null, null);
        this.species = Species.UNKNOWN;
    }
    public abstract void respond();
    public void eat(){
        Console.println("Я кушаю!");
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(Integer trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return species +
                "{nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                ", " + species +" "+ species.getCanFly() + ", " + species.getNumberOfLegs() +
                " and " + species.getHasFur() +
                '}';
    }
    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null) return false;
        if (!(that instanceof Pet)) return false;
        Pet pet = (Pet) that;
        if (!this.species.equals(pet.species)) return false;
        if (!this.nickname.equals(pet.nickname)) return false;
        if (!this.age.equals(pet.age)) return false;
        return true;
    }


    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age);
    }


    @Override
    public void finalize(){
        System.out.println("Object: " + this + " is deleted");
    }
}
