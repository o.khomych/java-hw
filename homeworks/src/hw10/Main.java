package hw10;

import libs.Console;
import libs.DayOfWeek;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<String> habits = new HashSet<String>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");
        Set<String> fishHabits = new HashSet<String>();
        fishHabits.add("eat");
        fishHabits.add("sleep");
        fishHabits.add("swim");

        Pet catMarci = new DomesticCat("Marci",2,80, habits);
        Pet fishDory = new Fish("Dory",1,10, fishHabits);

        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "do home work");
        schedule.put(DayOfWeek.FRIDAY, "write an assay");

        Woman LilyEvans = new Woman("Lily", "Evans", "12/01/1994", 124, schedule);
        Man MetthewJames = new Man("Metthew", "James", "12/01/1990", 115, schedule);

        Woman KateNoty = new Woman("Kate", "Noty", "12/01/1993", 134, schedule);
        Man BobySmith = new Man("Boby", "Smith", "12/01/1980", 113, schedule);

        Woman CaraWilly = new Woman("Cara", "Willy", "12/01/1983", 112, schedule);
        Man BillMilligan = new Man("Bill", "Milligan", "12/01/1989", 121, schedule);

        Woman SaraConor = new Woman("Sara", "Conor", "12/01/1987", 109, schedule);
        Man FillWeber = new Man("Fill", "Werb", "12/01/1987", 109, schedule);

        Woman AnnDary = new Woman("Ann", "Dary", "12/01/1986", 90, schedule);
        Man DanTolos = new Man("Dan", "Tolos", "12/01/1986", 89, schedule);


        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.createNewFamily(LilyEvans, MetthewJames);
        familyController.createNewFamily(KateNoty, BobySmith);
        familyController.createNewFamily(CaraWilly, BillMilligan);
        familyController.createNewFamily(SaraConor, FillWeber);
        familyController.createNewFamily(AnnDary, DanTolos);
        Console.println("--------------------------------------------------------------------------------------------");
        Console.println("Creating families, adding to DB, DisplayAllFamiles()");
        List<Family> allFamilies = familyController.getAllFamilies();
        familyController.displayAllFamilies();

        Family James = familyController.getFamilyById(0);
        Family Smith = familyController.getFamilyById(1);
        Family Milligan =familyController.getFamilyById(2);
        Family Weber = familyController.getFamilyById(3);
        Family Tolos = familyController.getFamilyById(4);

        Console.println("--------------------------------------------------------------------------------------------");
        Console.println("Get Family by index");
        Console.println(Milligan.toString());

        familyController.bornChild(James, "Adama", "Eva");
        familyController.bornChild(James, "Bob", "Dina");
        familyController.adoptChild(Weber, new Woman("Gabriel", "Dorn", "02/02/2018"));
        familyController.bornChild(Tolos, "James", "Hanna");
        familyController.adoptChild(Milligan, new Man("Tom", "Dorn", "03/03/2008"));
        familyController.adoptChild(Milligan, new Man("Ross", "Dorn", "05/01/2008"));

        Console.println("--------------------------------------------------------------------------------------------");
        Console.println("born Child, adoptChild");
        Console.println(James.toString());

        Console.println("--------------------------------------------------------------------------------------------");
        System.out.printf("Count of families with size 3: %d\n", familyController.countFamiliesWithMemberNumber(3));
        Console.println("Families: size bigger than 3");
        List<Family> familiesSizeBigger3 = familyController.getFamiliesBiggerThan(3);
        Console.println("Families: size less than 3");
        List<Family> familiesSizeLess3 = familyController.getFamiliesLessThan(3);


        familyController.addPet(3, catMarci);
        familyController.addPet(3, catMarci);
        familyController.addPet(3, fishDory);
        familyController.addPet(2, fishDory);

        Set<Pet> pets0 = familyController.getPets(0);
        Set<Pet> pets3 = familyController.getPets(3);
        Set<Pet> pets2 = familyController.getPets(2);

        Console.println("--------------------------------------------------------------------------------------------");
        Console.println("getting pets");
        Console.println(pets0.toString());
        Console.println(pets3.toString());
        Console.println(pets2.toString());

        int count = familyController.count();
        Console.println("--------------------------------------------------------------------------------------------");
        System.out.printf("%d\n", count);



        Console.println("--------------------------------------------------------------------------------------------");
        Console.println("before deleting children and Family3");
        familyController.displayAllFamilies();
        familyController.deleteAllChildrenOlderThen(10);

        Console.println("--------------------------------------------------------------------------------------------");
        Console.println("after deleting children");
        familyController.displayAllFamilies();

        familyController.deleteFamilyByIndex(3);
        Console.println("--------------------------------------------------------------------------------------------");
        Console.println("after deleting family");
        familyController.displayAllFamilies();
    }
}
