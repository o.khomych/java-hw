package hw5;

import libs.Console;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;
    public Family(Human mother, Human father){
        this.mother = mother;
        this.father = father;
        this.children = new Human[]{};
        mother.setFamily(this);
        father.setFamily(this);
    }
    public Human getMother(){return mother;}
    public Human getFather(){return father;}
    public Human[] getChildren(){return children;}
    public void setChildren(Human[] obj){children=obj;}
    public Pet getPet(){return pet;}
    public void setPet(Pet obj){pet=obj;}

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother.toString() +
                ", father=" + father.toString() +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }

    public void addChild(Human child){
        Human[] children = Arrays.copyOf(getChildren(), getChildren().length+1);
        children[children.length-1] = child;
        setChildren(children);
        child.setFamily(this);
    }

    public boolean deleteChildIndex(int index){
        Human[] children = getChildren();
        if (index > getChildren().length - 1) return false;
        children[index].setFamily(null);
        for (int i = index; i<children.length-1; i++){
            children[i] = children[i+1];
        }
        setChildren(Arrays.copyOf(children, children.length-1));
        return true;
    }

    public boolean deleteChildObj(Human child){
        Human[] children = getChildren();
        Integer index = null;
        boolean inArr = false;
        for (int i = 0; i < children.length; i++) {
            if (children[i].equals(child)) {
                index = i;
                children[i].setFamily(null);
                inArr = true;
            };
        }
        if (!inArr) return false;
        for (int i = index; i<children.length-1; i++){
            children[i] = children[i+1];
        }
        setChildren(Arrays.copyOf(children, children.length-1));
        return true;
    }

    public int countFamily(){
        return children.length + 2;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null) return false;
        if (!(that instanceof Family)) return false;
        Family family = (Family) that;
        if (!this.mother.equals(family.getMother())) return false;
        if (!this.father.equals(family.getFather())) return false;
        return true;
    }
    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }



//    static {
//        Console.println("new Family class loaded");
//    }
//    {
//        Console.println("creating new object type Family");
//    }
protected void finalize() throws Throwable {
    System.out.println("Object: " +this+" is deleted");
}
}
