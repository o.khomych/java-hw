package hw5;

public enum Species {
    DOG(false, 4, true),
    CAT(false, 4, true),
    FISH(false, 0, false),
    PARROT(true, 4, false),
    HAMSTER(false, 4, true),
    RABBIT(false, 4, true);

    private boolean canFly;
    private int numberOfLegs;
    private boolean hasFur;
    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }
    Species(){

    }
    public String getCanFly(){
        return canFly ? "can fly" : "cannot fly";
    }
    public String getNumberOfLegs(){
        return String.format("has %d legs", numberOfLegs);
    }
    public String getHasFur(){
        return hasFur ? "has fur" : "hasn't fur";
    }
}
