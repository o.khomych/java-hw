package hw5;

import hw8.FamilyController;
import hw8.FamilyDao;
import hw8.CollectionFamilyDao;
import hw8.FamilyService;
import libs.Console;


public class Main {
    public static void main(String[] args) {
        Pet catMarci = new Pet(Species.CAT, "Marci",2,80, new String[]{"eat", "drink", "sleep"});
        System.out.println(catMarci.toString());

        String[][] schedule = new String[][]{
                {DayOfWeek.MONDAY.name(),"do home work"},
                {DayOfWeek.TUESDAY.name(),"go to courses; watch a film"},
                {DayOfWeek.WEDNESDAY.name(), "go to the jym"},
                {DayOfWeek.THURSDAY.name(), "write an assay"},
                {DayOfWeek.FRIDAY.name(), "go to the doctor"},
                {DayOfWeek.SATURDAY.name(), "make a presentation"},
                {DayOfWeek.SUNDAY.name(), "read a book"}
        };
        Human KateSwan = new Human("Kate", "Swan", 1969, 98, schedule);
        System.out.println(KateSwan.toString());
        Console.println("--------------------------------------------------------------------------------------------");

        for (int
        i = 0; i < 1000000; i++) {
            new Human("Kate", "Swan", 1969, 98, schedule);
        }

        // FamilyController methods:

        Human LilyEvans = new Human("Lily", "Evans", 1994, 124, schedule);
        Human MetthewJames = new Human("Metthew", "James", 1990, 115, schedule);

        Human KateNoty = new Human("Kate", "Noty", 1993, 134, schedule);
        Human BobySmith = new Human("Boby", "Smith", 1980, 113, schedule);

        Human CaraWilly = new Human("Cara", "Willy", 1983, 112, schedule);
        Human BillMilligan = new Human("Bill", "Milligan", 1989, 121, schedule);

        Human SaraConor = new Human("Sara", "Conor", 1987, 109, schedule);
        Human FillWeber = new Human("Fill", "Werb", 1987, 109, schedule);

        Human AnnDary = new Human("Ann", "Dary", 1986, 90, schedule);
        Human DanTolos = new Human("Dan", "Tolos", 1986, 89, schedule);

        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);



//        familyController.createNewFamily(LilyEvans, MetthewJames);
//        familyController.createNewFamily(KateNoty, BobySmith);
//        familyController.createNewFamily(CaraWilly, BillMilligan);
//        familyController.createNewFamily(SaraConor, FillWeber);
//        familyController.createNewFamily(AnnDary, DanTolos);

    }
}
